-- phpMyAdmin SQL Dump
-- version 3.3.7deb3build0.10.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 16, 2011 at 09:00 PM
-- Server version: 5.1.49
-- PHP Version: 5.3.3-1ubuntu9.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_rel`
--

-- --------------------------------------------------------

--
-- Table structure for table `mascara_de_arquivo`
--

CREATE TABLE IF NOT EXISTS `mascara_de_arquivo` (
  `cod_mascara_de_arquivo` varchar(6) NOT NULL COMMENT 'Código da máscara de arquivo.',
  `num_versao_mascara_de_arquivo` smallint(3) unsigned NOT NULL DEFAULT '15',
  `dsc_mascara_de_arquivo` varchar(45) NOT NULL COMMENT 'Descrição da máscara de arquivo.',
  PRIMARY KEY (`cod_mascara_de_arquivo`,`num_versao_mascara_de_arquivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Repositório de máscaras para de arquivos.';

--
-- Dumping data for table `mascara_de_arquivo`
--

INSERT INTO `mascara_de_arquivo` (`cod_mascara_de_arquivo`, `num_versao_mascara_de_arquivo`, `dsc_mascara_de_arquivo`) VALUES
('A200', 15, 'ARQUIVO A200'),
('A500', 15, 'ARQUIVO A500');

-- --------------------------------------------------------

--
-- Table structure for table `mascara_de_arquivo_mascara_de_registro`
--

CREATE TABLE IF NOT EXISTS `mascara_de_arquivo_mascara_de_registro` (
  `cod_mascara_de_arquivo` varchar(6) NOT NULL,
  `num_versao_mascara_de_arquivo` smallint(3) unsigned NOT NULL DEFAULT '15',
  `cod_mascara_de_registro` varchar(6) NOT NULL,
  `num_versao_mascara_de_registro` smallint(3) unsigned NOT NULL DEFAULT '15',
  `num_sequencial_registro` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cod_mascara_de_arquivo`,`num_versao_mascara_de_arquivo`,`cod_mascara_de_registro`,`num_versao_mascara_de_registro`),
  KEY `fk_mascara_de_arquivo_registro_mascara_de_registro` (`cod_mascara_de_registro`,`num_versao_mascara_de_registro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mascara_de_arquivo_mascara_de_registro`
--

INSERT INTO `mascara_de_arquivo_mascara_de_registro` (`cod_mascara_de_arquivo`, `num_versao_mascara_de_arquivo`, `cod_mascara_de_registro`, `num_versao_mascara_de_registro`, `num_sequencial_registro`) VALUES
('A500', 15, '204', 15, 6),
('A500', 15, '501', 15, 1),
('A500', 15, '502', 15, 2),
('A500', 15, '503', 15, 3),
('A500', 15, '504', 15, 4),
('A500', 15, '505', 15, 5),
('A500', 15, '509', 15, 7);

-- --------------------------------------------------------

--
-- Table structure for table `mascara_de_campo`
--

CREATE TABLE IF NOT EXISTS `mascara_de_campo` (
  `cod_mascara_de_campo` varchar(30) NOT NULL,
  `cod_mascara_de_registro` varchar(6) NOT NULL,
  `num_versao_mascara_de_registro` smallint(3) unsigned NOT NULL DEFAULT '15',
  `vlr_posicao_inicial` decimal(4,0) unsigned NOT NULL DEFAULT '0',
  `vlr_posicao_final` decimal(4,0) unsigned NOT NULL DEFAULT '0',
  `vlr_tamanho` decimal(3,0) unsigned NOT NULL DEFAULT '0',
  `vlr_decimais` decimal(1,0) unsigned NOT NULL DEFAULT '0',
  `cod_tipo_de_origem` decimal(1,0) unsigned NOT NULL,
  `cod_tipo_de_campo` decimal(2,0) unsigned NOT NULL,
  `cod_tipo_de_uso` tinyint(1) unsigned NOT NULL,
  `ind_contem_tipo_de_registro` tinyint(1) NOT NULL DEFAULT '0',
  `ind_chave_primaria` tinyint(1) NOT NULL DEFAULT '0',
  `ind_contem_numero_de_versao` tinyint(1) NOT NULL DEFAULT '0',
  `num_sequencial_campo` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cod_mascara_de_campo`,`cod_mascara_de_registro`,`num_versao_mascara_de_registro`),
  KEY `fk_mascara_de_campo_mascara_de_registro` (`cod_mascara_de_registro`,`num_versao_mascara_de_registro`),
  KEY `fk_mascara_de_campo_tipo_de_origem` (`cod_tipo_de_origem`),
  KEY `fk_mascara_de_campo_tipo_de_campo` (`cod_tipo_de_campo`),
  KEY `fk_mascara_de_campo_tipo_de_uso` (`cod_tipo_de_uso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Repositório de máscaras para formatação de campos .';

--
-- Dumping data for table `mascara_de_campo`
--

INSERT INTO `mascara_de_campo` (`cod_mascara_de_campo`, `cod_mascara_de_registro`, `num_versao_mascara_de_registro`, `vlr_posicao_inicial`, `vlr_posicao_final`, `vlr_tamanho`, `vlr_decimais`, `cod_tipo_de_origem`, `cod_tipo_de_campo`, `cod_tipo_de_uso`, `ind_contem_tipo_de_registro`, `ind_chave_primaria`, `ind_contem_numero_de_versao`, `num_sequencial_campo`) VALUES
('CD_ATO', '504', 15, '242', '242', '1', '0', '1', '3', 0, 0, 0, 0, 30),
('CD_CID', '502', 15, '102', '107', '6', '0', '1', '13', 1, 0, 0, 0, 13),
('CD_CID_OBITO', '503', 15, '154', '159', '6', '0', '1', '13', 1, 0, 0, 0, 31),
('CD_CID_OBITO_1', '503', 15, '260', '265', '6', '0', '1', '13', 1, 0, 0, 0, 39),
('CD_CID_OBITO_2', '503', 15, '266', '271', '6', '0', '1', '13', 1, 0, 0, 0, 40),
('CD_CID_OBITO_3', '503', 15, '272', '277', '6', '0', '1', '13', 1, 0, 0, 0, 41),
('CD_CID_OBITO_4', '503', 15, '278', '283', '6', '0', '1', '13', 1, 0, 0, 0, 42),
('CD_CID_OBITO_5', '503', 15, '284', '289', '6', '0', '1', '13', 1, 0, 0, 0, 43),
('CD_CNES_PREST', '504', 15, '265', '271', '7', '0', '1', '3', 1, 0, 0, 0, 34),
('CD_ESPEC', '504', 15, '221', '222', '2', '0', '1', '3', 0, 0, 0, 0, 24),
('CD_EXCECAO', '502', 15, '98', '98', '1', '0', '1', '3', 0, 0, 0, 0, 11),
('CD_HOSP', '503', 15, '35', '42', '8', '0', '1', '3', 0, 0, 0, 0, 6),
('CD_PORTE_ANE', '504', 15, '151', '151', '1', '0', '1', '3', 1, 0, 0, 0, 16),
('CD_PREST', '504', 15, '35', '42', '8', '0', '1', '3', 0, 0, 0, 0, 6),
('CD_PRE_REQ', '504', 15, '169', '176', '8', '0', '1', '3', 1, 0, 0, 0, 19),
('CD_SERVICO', '504', 15, '93', '100', '8', '0', '1', '3', 0, 0, 0, 0, 11),
('CD_UNI', '502', 15, '32', '35', '4', '0', '1', '3', 0, 0, 0, 0, 7),
('CD_UNI_AUT', '504', 15, '152', '155', '4', '0', '1', '3', 1, 0, 0, 0, 17),
('CD_UNI_HOSP', '503', 15, '31', '34', '4', '0', '1', '3', 0, 0, 0, 0, 5),
('CD_UNI_ORI', '501', 15, '12', '15', '4', '0', '1', '3', 0, 0, 0, 0, 3),
('CD_UNI_PRE', '504', 15, '31', '34', '4', '0', '1', '3', 0, 0, 0, 0, 5),
('CD_UNI_PRE_REQ', '504', 15, '165', '168', '4', '0', '1', '3', 1, 0, 0, 0, 18),
('CD_VIA_ACESSO', '504', 15, '177', '178', '2', '0', '1', '3', 1, 0, 0, 0, 20),
('CGC_HOSPITAL', '503', 15, '119', '132', '14', '0', '1', '3', 0, 0, 0, 0, 14),
('CODIGO_CONTA', '204', 15, '70', '89', '20', '0', '1', '0', 0, 0, 0, 0, 11),
('COD_ESP_LOCAL', '204', 15, '106', '107', '2', '0', '1', '3', 1, 0, 0, 0, 17),
('COMPETENCIA', '204', 15, '58', '65', '8', '0', '1', '12', 0, 0, 0, 0, 8),
('DATA_REALIZACAO', '204', 15, '37', '57', '21', '0', '1', '11', 0, 0, 0, 0, 7),
('DT_ALTA', '503', 15, '91', '111', '21', '0', '1', '11', 0, 0, 0, 0, 10),
('DT_ATEND', '502', 15, '77', '97', '21', '0', '1', '11', 0, 0, 0, 0, 10),
('DT_GERACAO', '501', 15, '80', '87', '8', '0', '1', '12', 0, 0, 0, 0, 6),
('DT_INTERNACAO', '503', 15, '70', '90', '21', '0', '1', '11', 0, 0, 0, 0, 9),
('DT_SERVICO', '504', 15, '84', '91', '8', '0', '1', '12', 0, 0, 0, 0, 9),
('FAT_MULT_SERV', '504', 15, '243', '245', '1', '2', '1', '1', 0, 0, 0, 0, 31),
('FONTE', '502', 15, '0', '0', '1', '0', '0', '0', 0, 0, 0, 0, 1),
('FT_MULT_AMB', '503', 15, '112', '115', '2', '2', '1', '1', 0, 0, 0, 0, 11),
('GP_LOCAL', '204', 15, '104', '105', '2', '0', '1', '3', 1, 0, 0, 0, 16),
('HR_REALIZ', '504', 15, '257', '264', '8', '0', '1', '14', 0, 0, 0, 0, 33),
('ID_BENEF', '502', 15, '39', '51', '13', '0', '1', '3', 0, 0, 0, 0, 8),
('ID_INT_OBSTETRICIA_1', '503', 15, '144', '144', '1', '0', '1', '0', 1, 0, 0, 0, 21),
('ID_INT_OBSTETRICIA_2', '503', 15, '145', '145', '1', '0', '1', '0', 1, 0, 0, 0, 22),
('ID_INT_OBSTETRICIA_3', '503', 15, '146', '146', '1', '0', '1', '0', 1, 0, 0, 0, 23),
('ID_INT_OBSTETRICIA_4', '503', 15, '147', '147', '1', '0', '1', '0', 1, 0, 0, 0, 24),
('ID_INT_OBSTETRICIA_5', '503', 15, '148', '148', '1', '0', '1', '0', 1, 0, 0, 0, 25),
('ID_INT_OBSTETRICIA_6', '503', 15, '149', '149', '1', '0', '1', '0', 1, 0, 0, 0, 26),
('ID_INT_OBSTETRICIA_7', '503', 15, '150', '150', '1', '0', '1', '0', 1, 0, 0, 0, 27),
('ID_INT_OBSTETRICIA_8', '503', 15, '151', '151', '1', '0', '1', '0', 1, 0, 0, 0, 28),
('ID_INT_OBSTETRICIA_9', '503', 15, '152', '152', '1', '0', '1', '0', 1, 0, 0, 0, 29),
('ID_PACOTE', '504', 15, '241', '241', '1', '0', '1', '0', 0, 0, 0, 0, 29),
('ID_REC_PROPRIO', '504', 15, '225', '225', '1', '0', '1', '3', 0, 0, 0, 0, 26),
('ID_REEMBOLSO', '504', 15, '426', '426', '1', '0', '1', '0', 1, 0, 0, 0, 44),
('LOCAL_ATEND', '204', 15, '36', '36', '1', '0', '1', '1', 0, 0, 0, 0, 6),
('NM_BENEF', '502', 15, '52', '76', '25', '0', '1', '4', 1, 0, 0, 0, 9),
('NM_DESC_CPL', '505', 15, '32', '131', '100', '0', '1', '4', 0, 0, 0, 0, 6),
('NM_HOSP', '503', 15, '43', '67', '25', '0', '1', '4', 0, 0, 0, 0, 7),
('NM_PREST', '504', 15, '43', '82', '40', '0', '1', '4', 0, 0, 0, 0, 7),
('NM_PREST_REQ', '504', 15, '357', '396', '40', '0', '1', '4', 0, 0, 0, 0, 40),
('NM_PROF_PREST', '504', 15, '272', '311', '40', '0', '1', '4', 1, 0, 0, 0, 35),
('NR_AUTORIZ', '504', 15, '427', '436', '10', '0', '1', '3', 1, 0, 0, 0, 45),
('NR_CNPJ_CPF', '504', 15, '227', '240', '14', '0', '1', '3', 0, 0, 0, 0, 28),
('NR_CNPJ_CPF_REQ', '504', 15, '343', '356', '14', '0', '1', '3', 1, 0, 0, 0, 39),
('NR_COMP_FINAL', '501', 15, '22', '27', '6', '0', '1', '3', 0, 0, 0, 0, 5),
('NR_COMP_INICIAL', '501', 15, '16', '21', '6', '0', '1', '3', 0, 0, 0, 0, 4),
('NR_CONS_PROF_PREST', '504', 15, '324', '338', '15', '0', '1', '3', 1, 0, 0, 0, 37),
('NR_CONS_PROF_REQ', '504', 15, '409', '423', '15', '0', '1', '3', 0, 0, 0, 0, 42),
('NR_DECLARA_OBITO', '503', 15, '243', '259', '17', '0', '1', '3', 1, 0, 0, 0, 38),
('NR_DECLARA_OBITO_1', '503', 15, '290', '306', '17', '0', '1', '3', 1, 0, 0, 0, 44),
('NR_DECLARA_OBITO_2', '503', 15, '307', '323', '17', '0', '1', '3', 1, 0, 0, 0, 45),
('NR_DECLARA_OBITO_3', '503', 15, '324', '340', '17', '0', '1', '3', 1, 0, 0, 0, 46),
('NR_DECLARA_OBITO_4', '503', 15, '341', '357', '17', '0', '1', '3', 1, 0, 0, 0, 47),
('NR_DECLARA_OBITO_5', '503', 15, '358', '374', '17', '0', '1', '3', 1, 0, 0, 0, 48),
('NR_DECLARA_VIVO_1', '503', 15, '167', '181', '15', '0', '1', '3', 1, 0, 0, 0, 32),
('NR_DECLARA_VIVO_2', '503', 15, '182', '196', '15', '0', '1', '3', 1, 0, 0, 0, 33),
('NR_DECLARA_VIVO_3', '503', 15, '197', '211', '15', '0', '1', '3', 1, 0, 0, 0, 34),
('NR_DECLARA_VIVO_4', '503', 15, '212', '226', '15', '0', '1', '3', 1, 0, 0, 0, 35),
('NR_DECLARA_VIVO_5', '503', 15, '227', '241', '15', '0', '1', '3', 1, 0, 0, 0, 36),
('NR_GUIA_PRINCIPAL', '502', 15, '126', '136', '11', '0', '1', '3', 1, 0, 0, 0, 17),
('NR_LOTE', '204', 15, '12', '19', '8', '0', '1', '3', 0, 0, 0, 0, 3),
('NR_LOTE', '502', 15, '12', '19', '8', '0', '1', '3', 0, 0, 0, 0, 5),
('NR_LOTE', '503', 15, '12', '19', '8', '0', '1', '3', 0, 0, 0, 0, 3),
('NR_LOTE', '504', 15, '12', '19', '8', '0', '1', '3', 0, 0, 0, 0, 3),
('NR_LOTE', '505', 15, '12', '19', '8', '0', '1', '3', 0, 0, 0, 0, 3),
('NR_NOTA', '204', 15, '20', '30', '11', '0', '1', '3', 0, 0, 0, 0, 4),
('NR_NOTA', '502', 15, '20', '30', '11', '0', '1', '3', 0, 0, 0, 0, 6),
('NR_NOTA', '503', 15, '20', '30', '11', '0', '1', '3', 0, 0, 0, 0, 4),
('NR_NOTA', '504', 15, '20', '30', '11', '0', '1', '3', 0, 0, 0, 0, 4),
('NR_NOTA', '505', 15, '20', '30', '11', '0', '1', '3', 0, 0, 0, 0, 4),
('NR_SEQ', '204', 15, '1', '8', '8', '0', '1', '3', 0, 0, 0, 0, 1),
('NR_SEQ', '501', 15, '1', '8', '8', '0', '1', '3', 0, 0, 0, 0, 1),
('NR_SEQ', '502', 15, '1', '8', '8', '0', '1', '3', 0, 0, 0, 0, 3),
('NR_SEQ', '503', 15, '1', '8', '8', '0', '1', '3', 0, 0, 0, 0, 1),
('NR_SEQ', '504', 15, '1', '8', '8', '0', '1', '3', 0, 0, 0, 0, 1),
('NR_SEQ', '505', 15, '1', '8', '8', '0', '1', '3', 0, 0, 0, 0, 1),
('NR_SEQ', '509', 15, '1', '8', '8', '0', '1', '3', 0, 0, 0, 0, 1),
('NR_SEQ_NOTA', '504', 15, '246', '256', '11', '0', '1', '3', 0, 0, 0, 0, 32),
('NR_VER_TRA', '501', 15, '88', '89', '2', '0', '1', '3', 0, 0, 0, 0, 7),
('PERC_PARTICIPACAO', '204', 15, '97', '103', '5', '2', '1', '1', 1, 0, 0, 0, 15),
('PRINCIPAL', '204', 15, '96', '96', '1', '0', '1', '1', 0, 0, 0, 0, 14),
('QTE_M2_FILME', '204', 15, '174', '183', '6', '4', '1', '1', 0, 0, 0, 0, 24),
('QTE_UT_CO', '204', 15, '166', '173', '6', '2', '1', '1', 0, 0, 0, 0, 23),
('QTE_UT_HM', '204', 15, '158', '165', '6', '2', '1', '1', 0, 0, 0, 0, 22),
('QT_COBRADA', '504', 15, '101', '108', '8', '0', '1', '1', 0, 0, 0, 0, 12),
('QT_NASC_MORTOS', '503', 15, '138', '139', '2', '0', '1', '1', 1, 0, 0, 0, 17),
('QT_NASC_VIVOS', '503', 15, '136', '137', '2', '0', '1', '1', 1, 0, 0, 0, 16),
('QT_NASC_VIVOS_PRE', '503', 15, '140', '141', '2', '0', '1', '1', 1, 0, 0, 0, 18),
('QT_NOT_EXC', '509', 15, '32', '36', '5', '0', '1', '1', 0, 0, 0, 0, 7),
('QT_OBITO_PRECOCE', '503', 15, '142', '142', '1', '0', '1', '1', 1, 0, 0, 0, 19),
('QT_OBITO_TARDIO', '503', 15, '143', '143', '1', '0', '1', '1', 1, 0, 0, 0, 20),
('QT_PAGA', '204', 15, '108', '115', '4', '4', '1', '1', 0, 0, 0, 0, 18),
('QT_TOT_R502', '509', 15, '12', '16', '5', '0', '1', '1', 0, 0, 0, 0, 3),
('QT_TOT_R503', '509', 15, '17', '21', '5', '0', '1', '1', 0, 0, 0, 0, 4),
('QT_TOT_R504', '509', 15, '22', '26', '5', '0', '1', '1', 0, 0, 0, 0, 5),
('QT_TOT_R505', '509', 15, '27', '31', '5', '0', '1', '1', 0, 0, 0, 0, 6),
('QT_TOT_SER', '509', 15, '42', '52', '11', '0', '1', '1', 0, 0, 0, 0, 8),
('REFERENCIA', '502', 15, '0', '0', '6', '0', '0', '0', 0, 0, 0, 0, 2),
('SEQ_COMPETENCIA', '204', 15, '66', '67', '2', '0', '1', '3', 0, 0, 0, 0, 9),
('SEQ_CONTA', '204', 15, '90', '93', '4', '0', '1', '3', 0, 0, 0, 0, 12),
('SEQ_PAGAMENTO', '204', 15, '184', '184', '1', '0', '1', '0', 0, 0, 0, 0, 25),
('SG_CONS_PROF_PREST', '504', 15, '312', '323', '12', '0', '1', '3', 1, 0, 0, 0, 36),
('SG_CONS_PROF_REQ', '504', 15, '397', '408', '12', '0', '1', '3', 0, 0, 0, 0, 41),
('SG_UF_CONS_PREST', '504', 15, '339', '340', '2', '0', '1', '15', 1, 0, 0, 0, 38),
('SG_UF_CONS_REQ', '504', 15, '424', '425', '2', '0', '1', '15', 0, 0, 0, 0, 43),
('TIPO_COBRANCA', '204', 15, '94', '95', '2', '0', '1', '3', 0, 0, 0, 0, 13),
('TIPO_CONTA', '204', 15, '68', '69', '2', '0', '1', '0', 0, 0, 0, 0, 10),
('TIPO_SERVICO', '204', 15, '31', '35', '5', '0', '1', '0', 0, 0, 0, 0, 5),
('TP_ACOMODACAO', '503', 15, '68', '69', '2', '0', '1', '3', 0, 0, 0, 0, 8),
('TP_ATENDIMENTO', '502', 15, '124', '125', '2', '0', '1', '3', 0, 0, 0, 0, 16),
('TP_CARACTER_ATEND', '502', 15, '99', '99', '1', '0', '1', '3', 0, 0, 0, 0, 12),
('TP_FATURAMENTO', '503', 15, '153', '153', '1', '0', '1', '1', 0, 0, 0, 0, 30),
('TP_IND_ACIDENTE', '503', 15, '116', '116', '1', '0', '1', '3', 1, 0, 0, 0, 12),
('TP_INTERNACAO', '503', 15, '133', '133', '1', '0', '1', '1', 0, 0, 0, 0, 15),
('TP_OBITO_MULHER', '503', 15, '242', '242', '1', '0', '1', '1', 1, 0, 0, 0, 37),
('TP_PACIENTE', '502', 15, '122', '122', '1', '0', '1', '3', 0, 0, 0, 0, 14),
('TP_PARTICIP', '504', 15, '83', '83', '1', '0', '1', '3', 0, 0, 0, 0, 8),
('TP_PESSOA', '504', 15, '226', '226', '1', '0', '1', '3', 0, 0, 0, 0, 27),
('TP_PREST_EXEC', '504', 15, '223', '224', '2', '0', '1', '3', 0, 0, 0, 0, 25),
('TP_REG', '204', 15, '9', '11', '3', '0', '1', '3', 0, 1, 0, 0, 2),
('TP_REG', '501', 15, '9', '11', '3', '0', '1', '3', 0, 1, 0, 0, 2),
('TP_REG', '502', 15, '9', '11', '3', '0', '1', '3', 0, 1, 0, 0, 4),
('TP_REG', '503', 15, '9', '11', '3', '0', '1', '3', 0, 1, 0, 0, 2),
('TP_REG', '504', 15, '9', '11', '3', '0', '1', '3', 0, 1, 0, 0, 2),
('TP_REG', '505', 15, '9', '11', '3', '0', '1', '3', 0, 1, 0, 0, 2),
('TP_REG', '509', 15, '9', '11', '3', '0', '1', '3', 0, 1, 0, 0, 2),
('TP_REG_CPL', '505', 15, '31', '31', '1', '0', '1', '1', 0, 0, 0, 0, 5),
('TP_SAIDA', '503', 15, '117', '118', '2', '0', '1', '3', 0, 0, 0, 0, 13),
('TP_SAIDA_CONS_SADT', '502', 15, '123', '123', '1', '0', '1', '3', 0, 0, 0, 0, 15),
('TP_TABELA', '504', 15, '92', '92', '1', '0', '1', '1', 0, 0, 0, 0, 10),
('VL_ADIC_CO', '504', 15, '193', '206', '12', '2', '1', '1', 1, 0, 0, 0, 22),
('VL_ADIC_FILME', '504', 15, '207', '220', '12', '2', '1', '1', 1, 0, 0, 0, 23),
('VL_ADIC_SER', '504', 15, '179', '192', '12', '2', '1', '1', 1, 0, 0, 0, 21),
('VL_CO_COB', '504', 15, '123', '136', '12', '2', '1', '1', 1, 0, 0, 0, 14),
('VL_CO_PAGO', '204', 15, '130', '143', '12', '2', '1', '1', 1, 0, 0, 0, 20),
('VL_FILME_COB', '504', 15, '137', '150', '12', '2', '1', '1', 1, 0, 0, 0, 15),
('VL_FILME_PAGO', '204', 15, '144', '157', '12', '2', '1', '1', 1, 0, 0, 0, 21),
('VL_SERVICO_PAGO', '204', 15, '116', '129', '12', '2', '1', '1', 1, 0, 0, 0, 19),
('VL_SERV_COB', '504', 15, '109', '122', '12', '2', '1', '1', 1, 0, 0, 0, 13),
('VL_TOT_SERV', '509', 15, '53', '64', '12', '2', '1', '1', 0, 0, 0, 0, 9);

-- --------------------------------------------------------

--
-- Table structure for table `mascara_de_registro`
--

CREATE TABLE IF NOT EXISTS `mascara_de_registro` (
  `cod_mascara_de_registro` varchar(6) NOT NULL,
  `num_versao_mascara_de_registro` smallint(3) unsigned NOT NULL DEFAULT '15',
  `dsc_mascara_de_registro` varchar(45) NOT NULL,
  `vlr_tamanho` decimal(4,0) unsigned NOT NULL,
  PRIMARY KEY (`cod_mascara_de_registro`,`num_versao_mascara_de_registro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mascara_de_registro`
--

INSERT INTO `mascara_de_registro` (`cod_mascara_de_registro`, `num_versao_mascara_de_registro`, `dsc_mascara_de_registro`, `vlr_tamanho`) VALUES
('204', 15, 'R204 - COMPLEMENTAR DE SERVICOS', '0'),
('501', 15, 'R501 - HEADER', '84'),
('502', 15, 'R502 - NOTA DE COBRANCA', '108'),
('503', 15, 'R503 - HOSPITALAR', '363'),
('504', 15, 'R504 - SERVICO', '0'),
('505', 15, 'R505 - COMPLEMENTAR', '0'),
('509', 15, 'R509 - TRAILER', '59');

-- --------------------------------------------------------

--
-- Table structure for table `pendencia_de_alteracao`
--

CREATE TABLE IF NOT EXISTS `pendencia_de_alteracao` (
  `num_sequencial_pendencia` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cod_mascara_de_campo` varchar(30) NOT NULL,
  `cod_mascara_de_registro` varchar(6) NOT NULL,
  `num_versao_mascara_de_registro` smallint(3) NOT NULL,
  `cod_tipo_de_pendencia` decimal(1,0) unsigned NOT NULL,
  `dat_pendencia_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ind_pendencia_resolvida` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`num_sequencial_pendencia`),
  KEY `fk_pendencia_de_alteracao_mascara_de_campo` (`cod_mascara_de_campo`,`cod_mascara_de_registro`),
  KEY `fk_pendencia_de_alteracao_tipo_de_pendencia1` (`cod_tipo_de_pendencia`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pendencia_de_alteracao`
--

INSERT INTO `pendencia_de_alteracao` (`num_sequencial_pendencia`, `cod_mascara_de_campo`, `cod_mascara_de_registro`, `num_versao_mascara_de_registro`, `cod_tipo_de_pendencia`, `dat_pendencia_alteracao`, `ind_pendencia_resolvida`) VALUES
(1, 'NR_TESTE', '502', 0, '0', '2011-01-05 19:04:13', 1),
(3, 'NR_TESTE', '502', 0, '0', '2011-01-06 19:10:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tipo_de_campo`
--

CREATE TABLE IF NOT EXISTS `tipo_de_campo` (
  `cod_tipo_de_campo` decimal(2,0) unsigned NOT NULL DEFAULT '0',
  `dsc_tipo_de_campo` varchar(45) NOT NULL,
  `cod_tipo_de_campo_no_destino` varchar(25) NOT NULL,
  `dsc_formato_tipo_de_campo` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`cod_tipo_de_campo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_de_campo`
--

INSERT INTO `tipo_de_campo` (`cod_tipo_de_campo`, `dsc_tipo_de_campo`, `cod_tipo_de_campo_no_destino`, `dsc_formato_tipo_de_campo`) VALUES
('0', 'ALFABETICO', 'VARCHAR', NULL),
('1', 'NUMERICO NAO SINALIZADO', 'DECIMAL', NULL),
('2', 'NUMERICO SINALIZADO', 'DECIMAL', NULL),
('3', 'ALFANUMERICO', 'VARCHAR', NULL),
('4', 'ALFANUMERICO COM CARACTERS ESPECIAIS', 'VARCHAR', NULL),
('5', 'MES DE 1 A 12', 'DECIMAL', 'NN'),
('6', 'DIA DE 1 A 31', 'DECIMAL', 'NN'),
('7', 'ANO DE 1900 a 2999', 'DECIMAL', 'NNNN'),
('8', 'HORA LOCAL DE 00 A 23', 'DECIMAL', 'NN'),
('9', 'Minutos Local de 00 a 59', 'DECIMAL', '99'),
('10', 'Segundos Local de 00 a 59', 'DECIMAL', '99'),
('11', 'DATA COM HORA MINUTO E SEGUNDO', 'VARCHAR', 'YYYY/MM/DDHH:MM:SS'),
('12', 'DATA ', 'DATE', 'YYYYMMDD'),
('13', 'CID', 'VARCHAR', 'ANNC'),
('14', 'HORA', 'TIME', 'HH:MM:SS'),
('15', 'UF', 'CHAR', NULL),
('90', 'MINUTOS LOCAL DE 00 A 59', 'DECIMAL', '99'),
('91', 'SEGUNDOS LOCAL DE 00 A 59', 'DECIMAL', '99');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_de_origem`
--

CREATE TABLE IF NOT EXISTS `tipo_de_origem` (
  `cod_tipo_de_origem` decimal(1,0) unsigned NOT NULL,
  `dsc_tipo_de_origem` varchar(45) NOT NULL,
  PRIMARY KEY (`cod_tipo_de_origem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_de_origem`
--

INSERT INTO `tipo_de_origem` (`cod_tipo_de_origem`, `dsc_tipo_de_origem`) VALUES
('0', 'PARAMETRO'),
('1', 'REGISTRO');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_de_pendencia`
--

CREATE TABLE IF NOT EXISTS `tipo_de_pendencia` (
  `cod_tipo_de_pendencia` decimal(1,0) unsigned NOT NULL,
  `dsc_tipo_de_pendencia` varchar(45) NOT NULL,
  PRIMARY KEY (`cod_tipo_de_pendencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_de_pendencia`
--

INSERT INTO `tipo_de_pendencia` (`cod_tipo_de_pendencia`, `dsc_tipo_de_pendencia`) VALUES
('1', 'INCLUSAO'),
('2', 'ALTERACAO'),
('3', 'EXCLUSAO');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_de_uso`
--

CREATE TABLE IF NOT EXISTS `tipo_de_uso` (
  `cod_tipo_de_uso` tinyint(1) unsigned NOT NULL,
  `dsc_tipo_de_uso` varchar(45) NOT NULL,
  PRIMARY KEY (`cod_tipo_de_uso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_de_uso`
--

INSERT INTO `tipo_de_uso` (`cod_tipo_de_uso`, `dsc_tipo_de_uso`) VALUES
(0, 'MANDATORIO'),
(1, 'OPCIONAL');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mascara_de_arquivo_mascara_de_registro`
--
ALTER TABLE `mascara_de_arquivo_mascara_de_registro`
  ADD CONSTRAINT `fk_mascara_de_arquivo_registro_mascara_de_arquivo` FOREIGN KEY (`cod_mascara_de_arquivo`, `num_versao_mascara_de_arquivo`) REFERENCES `mascara_de_arquivo` (`cod_mascara_de_arquivo`, `num_versao_mascara_de_arquivo`),
  ADD CONSTRAINT `fk_mascara_de_arquivo_registro_mascara_de_registro` FOREIGN KEY (`cod_mascara_de_registro`, `num_versao_mascara_de_registro`) REFERENCES `mascara_de_registro` (`cod_mascara_de_registro`, `num_versao_mascara_de_registro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mascara_de_campo`
--
ALTER TABLE `mascara_de_campo`
  ADD CONSTRAINT `fk_mascara_de_campo_mascara_de_registro` FOREIGN KEY (`cod_mascara_de_registro`, `num_versao_mascara_de_registro`) REFERENCES `mascara_de_registro` (`cod_mascara_de_registro`, `num_versao_mascara_de_registro`),
  ADD CONSTRAINT `fk_mascara_de_campo_tipo_de_campo` FOREIGN KEY (`cod_tipo_de_campo`) REFERENCES `tipo_de_campo` (`cod_tipo_de_campo`),
  ADD CONSTRAINT `fk_mascara_de_campo_tipo_de_origem` FOREIGN KEY (`cod_tipo_de_origem`) REFERENCES `tipo_de_origem` (`cod_tipo_de_origem`),
  ADD CONSTRAINT `fk_mascara_de_campo_tipo_de_uso` FOREIGN KEY (`cod_tipo_de_uso`) REFERENCES `tipo_de_uso` (`cod_tipo_de_uso`);

--
-- Constraints for table `pendencia_de_alteracao`
--
ALTER TABLE `pendencia_de_alteracao`
  ADD CONSTRAINT `fk_pendencia_de_alteracao_mascara_de_campo` FOREIGN KEY (`cod_mascara_de_campo`, `cod_mascara_de_registro`) REFERENCES `mascara_de_campo` (`cod_mascara_de_campo`, `cod_mascara_de_registro`),
  ADD CONSTRAINT `fk_pendencia_de_alteracao_tipo_de_pendencia1` FOREIGN KEY (`cod_tipo_de_pendencia`) REFERENCES `tipo_de_pendencia` (`cod_tipo_de_pendencia`) ON DELETE NO ACTION ON UPDATE NO ACTION;
