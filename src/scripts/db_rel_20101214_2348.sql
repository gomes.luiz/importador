-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.49-1ubuntu8.1


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema db_rel
--

CREATE DATABASE IF NOT EXISTS db_rel;
USE db_rel;

--
-- Definition of table `db_rel`.`mascara_de_arquivo`
--

DROP TABLE IF EXISTS `db_rel`.`mascara_de_arquivo`;
CREATE TABLE  `db_rel`.`mascara_de_arquivo` (
  `cod_mascara_de_arquivo` varchar(6) NOT NULL,
  `dsc_mascara_de_arquivo` varchar(45) NOT NULL,
  `cod_tipo_de_categoria` decimal(1,0) NOT NULL,
  PRIMARY KEY (`cod_mascara_de_arquivo`),
  KEY `fk_mascara_de_arquivo_tipo_de_categoria1` (`cod_tipo_de_categoria`),
  CONSTRAINT `fk_mascara_de_arquivo_tipo_de_categoria` FOREIGN KEY (`cod_tipo_de_categoria`) REFERENCES `tipo_de_categoria` (`cod_tipo_de_categoria`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_rel`.`mascara_de_arquivo`
--
INSERT INTO `db_rel`.`mascara_de_arquivo` VALUES  ('A200','ARQUIVO A200','0'),
 ('A500','ARQUIVO A500','0');

--
-- Definition of table `db_rel`.`mascara_de_arquivo_mascara_de_registro`
--

DROP TABLE IF EXISTS `db_rel`.`mascara_de_arquivo_mascara_de_registro`;
CREATE TABLE  `db_rel`.`mascara_de_arquivo_mascara_de_registro` (
  `cod_mascara_de_arquivo` varchar(6) NOT NULL,
  `cod_mascara_de_registro` varchar(6) NOT NULL,
  PRIMARY KEY (`cod_mascara_de_arquivo`,`cod_mascara_de_registro`),
  KEY `fk_table1_mascara_de_registro1` (`cod_mascara_de_registro`),
  CONSTRAINT `fk_table1_mascara_de_arquivo1` FOREIGN KEY (`cod_mascara_de_arquivo`) REFERENCES `mascara_de_arquivo` (`cod_mascara_de_arquivo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_mascara_de_registro1` FOREIGN KEY (`cod_mascara_de_registro`) REFERENCES `mascara_de_registro` (`cod_mascara_de_registro`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_rel`.`mascara_de_arquivo_mascara_de_registro`
--
INSERT INTO `db_rel`.`mascara_de_arquivo_mascara_de_registro` VALUES  ('A500','501'),
 ('A500','502'),
 ('A500','509');

--
-- Definition of table `db_rel`.`mascara_de_campo`
--

DROP TABLE IF EXISTS `db_rel`.`mascara_de_campo`;
CREATE TABLE  `db_rel`.`mascara_de_campo` (
  `cod_mascara_de_campo` varchar(30) NOT NULL,
  `cod_mascara_de_registro` varchar(6) NOT NULL,
  `vlr_posicao_inicial` decimal(4,0) NOT NULL DEFAULT '0',
  `vlr_posicao_final` decimal(4,0) NOT NULL DEFAULT '0',
  `vlr_tamanho` decimal(3,0) NOT NULL DEFAULT '0',
  `vlr_decimais` decimal(1,0) NOT NULL DEFAULT '0',
  `cod_tipo_de_origem` decimal(1,0) NOT NULL,
  `cod_tipo_de_campo` decimal(2,0) NOT NULL,
  `cod_tipo_de_uso` decimal(1,0) NOT NULL,
  `ind_contem_tipo_de_registro` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cod_mascara_de_campo`,`cod_mascara_de_registro`),
  KEY `fk_mascara_de_campo_mascara_de_registro` (`cod_mascara_de_registro`),
  KEY `fk_mascara_de_campo_tipo_de_origem1` (`cod_tipo_de_origem`),
  KEY `fk_mascara_de_campo_tipo_de_campo1` (`cod_tipo_de_campo`),
  KEY `fk_mascara_de_campo_tipo_de_uso1` (`cod_tipo_de_uso`),
  CONSTRAINT `fk_mascara_de_campo_mascara_de_registro` FOREIGN KEY (`cod_mascara_de_registro`) REFERENCES `mascara_de_registro` (`cod_mascara_de_registro`),
  CONSTRAINT `fk_mascara_de_campo_tipo_de_campo` FOREIGN KEY (`cod_tipo_de_campo`) REFERENCES `tipo_de_campo` (`cod_tipo_de_campo`),
  CONSTRAINT `fk_mascara_de_campo_tipo_de_origem` FOREIGN KEY (`cod_tipo_de_origem`) REFERENCES `tipo_de_origem` (`cod_tipo_de_origem`),
  CONSTRAINT `fk_mascara_de_campo_tipo_de_uso` FOREIGN KEY (`cod_tipo_de_uso`) REFERENCES `tipo_de_uso` (`cod_tipo_de_uso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_rel`.`mascara_de_campo`
--
INSERT INTO `db_rel`.`mascara_de_campo` VALUES  ('CD_ATO','504','242','242','1','0','1','3','0',0),
 ('CD_CID','502','102','107','6','0','1','13','1',0),
 ('CD_CID_OBITO','503','154','159','6','0','1','13','1',0),
 ('CD_CID_OBITO_1','503','260','265','6','0','1','13','1',0),
 ('CD_CID_OBITO_2','503','266','271','6','0','1','13','1',0),
 ('CD_CID_OBITO_3','503','272','277','6','0','1','13','1',0),
 ('CD_CID_OBITO_4','503','278','283','6','0','1','13','1',0),
 ('CD_CID_OBITO_5','503','284','289','6','0','1','13','1',0),
 ('CD_CNES_PREST','504','265','271','7','0','1','3','1',0),
 ('CD_ESPEC','504','221','222','2','0','1','1','0',0),
 ('CD_EXCECAO','502','98','98','1','0','1','3','0',0),
 ('CD_HOSP','503','35','42','8','0','1','1','0',0),
 ('CD_PORTE_ANE','504','151','151','1','0','1','3','1',0),
 ('CD_PREST','504','35','42','8','0','1','1','0',0),
 ('CD_PRE_REQ','504','169','176','8','0','1','1','1',0),
 ('CD_SERVICO','504','93','100','8','0','1','1','0',0),
 ('CD_UNI','502','32','35','4','0','1','1','0',0),
 ('CD_UNI_AUT','504','152','155','4','0','1','1','1',0),
 ('CD_UNI_HOSP','503','31','34','4','0','1','1','0',0),
 ('CD_UNI_ORI','501','12','15','4','0','1','1','0',0),
 ('CD_UNI_PRE','504','31','34','4','0','1','1','0',0),
 ('CD_UNI_PRE_REQ','504','165','168','4','0','1','1','1',0),
 ('CD_VIA_ACESSO','504','177','178','2','0','1','1','1',0),
 ('CGC_HOSPITAL','503','119','132','14','0','1','1','0',0),
 ('CODIGO_CONTA','204','70','89','20','0','1','0','0',0),
 ('COD_ESP_LOCAL','204','106','107','2','0','1','1','1',0),
 ('COMPETENCIA','204','58','65','8','0','1','12','0',0),
 ('DATA_REALIZACAO','204','37','57','21','0','1','11','0',0),
 ('DT_ALTA','503','91','111','21','0','1','11','0',0),
 ('DT_ATEND','502','77','97','21','0','1','11','0',0),
 ('DT_GERACAO','501','80','87','8','0','1','12','0',0),
 ('DT_INTERNACAO','503','70','90','21','0','1','11','0',0),
 ('DT_SERVICO','504','84','91','8','0','1','12','0',0),
 ('FAT_MULT_SERV','504','243','245','1','2','1','1','0',0),
 ('FONTE','502','0','0','1','0','0','0','0',0),
 ('FT_MULT_AMB','503','112','115','2','2','1','1','0',0),
 ('GP_LOCAL','204','104','105','2','0','1','1','1',0),
 ('HR_REALIZ','504','257','264','8','0','1','14','0',0),
 ('ID_BENEF','502','39','51','13','0','1','1','0',0),
 ('ID_INT_OBSTETRICIA_1','503','144','144','1','0','1','0','1',0),
 ('ID_INT_OBSTETRICIA_2','503','145','145','1','0','1','0','1',0),
 ('ID_INT_OBSTETRICIA_3','503','146','146','1','0','1','0','1',0),
 ('ID_INT_OBSTETRICIA_4','503','147','147','1','0','1','0','1',0),
 ('ID_INT_OBSTETRICIA_5','503','148','148','1','0','1','0','1',0),
 ('ID_INT_OBSTETRICIA_6','503','149','149','1','0','1','0','1',0),
 ('ID_INT_OBSTETRICIA_7','503','150','150','1','0','1','0','1',0),
 ('ID_INT_OBSTETRICIA_8','503','151','151','1','0','1','0','1',0),
 ('ID_INT_OBSTETRICIA_9','503','152','152','1','0','1','0','1',0),
 ('ID_PACOTE','504','241','241','1','0','1','0','0',0),
 ('ID_REC_PROPRIO','504','225','225','1','0','1','3','0',0),
 ('ID_REEMBOLSO','504','426','426','1','0','1','0','1',0),
 ('LOCAL_ATEND','204','36','36','1','0','1','1','0',0),
 ('NM_BENEF','502','52','76','25','0','1','4','1',0),
 ('NM_DESC_CPL','505','32','131','100','0','1','4','0',0),
 ('NM_HOSP','503','43','67','25','0','1','4','0',0),
 ('NM_PREST','504','43','82','40','0','1','4','0',0),
 ('NM_PREST_REQ','504','357','396','40','0','1','4','0',0),
 ('NM_PROF_PREST','504','272','311','40','0','1','4','1',0),
 ('NR_AUTORIZ','504','427','436','10','0','1','1','1',0),
 ('NR_CNPJ_CPF','504','227','240','14','0','1','1','0',0),
 ('NR_CNPJ_CPF_REQ','504','343','356','14','0','1','1','1',0),
 ('NR_COMP_FINAL','501','22','27','6','0','1','1','0',0),
 ('NR_COMP_INICIAL','501','16','21','6','0','1','1','0',0),
 ('NR_CONS_PROF_PREST','504','324','338','15','0','1','3','1',0),
 ('NR_CONS_PROF_REQ','504','409','423','15','0','1','3','0',0),
 ('NR_DECLARA_OBITO','503','243','259','17','0','1','3','1',0),
 ('NR_DECLARA_OBITO_1','503','290','306','17','0','1','3','1',0),
 ('NR_DECLARA_OBITO_2','503','307','323','17','0','1','3','1',0),
 ('NR_DECLARA_OBITO_3','503','324','340','17','0','1','3','1',0),
 ('NR_DECLARA_OBITO_4','503','341','357','17','0','1','3','1',0),
 ('NR_DECLARA_OBITO_5','503','358','374','17','0','1','3','1',0),
 ('NR_DECLARA_VIVO_1','503','167','181','15','0','1','3','1',0),
 ('NR_DECLARA_VIVO_2','503','182','196','15','0','1','3','1',0),
 ('NR_DECLARA_VIVO_3','503','197','211','15','0','1','3','1',0),
 ('NR_DECLARA_VIVO_4','503','212','226','15','0','1','3','1',0),
 ('NR_DECLARA_VIVO_5','503','227','241','15','0','1','3','1',0),
 ('NR_GUIA_PRINCIPAL','502','126','136','11','0','1','1','1',0),
 ('NR_LOTE','204','12','19','8','0','1','1','0',0),
 ('NR_LOTE','502','12','19','8','0','1','1','0',0),
 ('NR_LOTE','503','12','19','8','0','1','1','0',0),
 ('NR_LOTE','504','12','19','8','0','1','1','0',0),
 ('NR_LOTE','505','12','19','8','0','1','1','0',0),
 ('NR_NOTA','204','20','30','11','0','1','1','0',0),
 ('NR_NOTA','502','20','30','11','0','1','1','0',0),
 ('NR_NOTA','503','20','30','11','0','1','1','0',0),
 ('NR_NOTA','504','20','30','11','0','1','1','0',0),
 ('NR_NOTA','505','20','30','11','0','1','1','0',0),
 ('NR_SEQ','204','1','8','8','0','1','1','0',0),
 ('NR_SEQ','501','1','8','8','0','1','1','0',0),
 ('NR_SEQ','502','1','8','8','0','1','1','0',0),
 ('NR_SEQ','503','1','8','8','0','1','1','0',0),
 ('NR_SEQ','504','1','8','8','0','1','1','0',0),
 ('NR_SEQ','505','1','8','8','0','1','1','0',0),
 ('NR_SEQ','509','1','8','8','0','1','1','0',0),
 ('NR_SEQ_NOTA','504','246','256','11','0','1','1','0',0),
 ('NR_VER_TRA','501','88','89','2','0','1','1','0',0),
 ('PERC_PARTICIPACAO','204','97','103','5','2','1','1','1',0),
 ('PRINCIPAL','204','96','96','1','0','1','1','0',0),
 ('QTE_M2_FILME','204','174','183','6','4','1','1','0',0),
 ('QTE_UT_CO','204','166','173','6','2','1','1','0',0),
 ('QTE_UT_HM','204','158','165','6','2','1','1','0',0),
 ('QT_COBRADA','504','101','108','8','0','1','1','0',0),
 ('QT_NASC_MORTOS','503','138','139','2','0','1','1','1',0),
 ('QT_NASC_VIVOS','503','136','137','2','0','1','1','1',0),
 ('QT_NASC_VIVOS_PRE','503','140','141','2','0','1','1','1',0),
 ('QT_NOT_EXC','509','32','36','5','0','1','1','0',0),
 ('QT_OBITO_PRECOCE','503','142','142','1','0','1','1','1',0),
 ('QT_OBITO_TARDIO','503','143','143','1','0','1','1','1',0),
 ('QT_PAGA','204','108','115','4','2','1','1','0',0),
 ('QT_TOT_R502','509','12','16','5','0','1','1','0',0),
 ('QT_TOT_R503','509','17','21','5','0','1','1','0',0),
 ('QT_TOT_R504','509','22','26','5','0','1','1','0',0),
 ('QT_TOT_R505','509','27','31','5','0','1','1','0',0),
 ('QT_TOT_SER','509','42','52','11','0','1','1','0',0),
 ('REFERENCIA','502','0','0','6','0','0','0','0',0),
 ('SEQ_COMPETENCIA','204','66','67','2','0','1','1','0',0),
 ('SEQ_CONTA','204','90','93','4','0','1','1','0',0),
 ('SEQ_PAGAMENTO','204','184','184','1','0','1','0','0',0),
 ('SG_CONS_PROF_PREST','504','312','323','12','0','1','3','1',0),
 ('SG_CONS_PROF_REQ','504','397','408','12','0','1','3','0',0),
 ('SG_UF_CONS_PREST','504','339','340','2','0','1','15','1',0),
 ('SG_UF_CONS_REQ','504','424','425','2','0','1','15','0',0),
 ('TIPO_COBRANCA','204','94','95','2','0','1','1','0',0),
 ('TIPO_CONTA','204','68','69','2','0','1','0','0',0),
 ('TIPO_SERVICO','204','31','35','5','0','1','0','0',0),
 ('TP_ACOMODACAO','503','68','69','2','0','1','3','0',0),
 ('TP_ATENDIMENTO','502','124','125','2','0','1','3','0',0),
 ('TP_CARACTER_ATEND','502','99','99','1','0','1','3','0',0),
 ('TP_FATURAMENTO','503','153','153','1','0','1','1','0',0),
 ('TP_IND_ACIDENTE','503','116','116','1','0','1','3','1',0),
 ('TP_INTERNACAO','503','133','133','1','0','1','1','0',0),
 ('TP_OBITO_MULHER','503','242','242','1','0','1','1','1',0),
 ('TP_PACIENTE','502','122','122','1','0','1','3','0',0),
 ('TP_PARTICIP','504','83','83','1','0','1','3','0',0),
 ('TP_PESSOA','504','226','226','1','0','1','3','0',0),
 ('TP_PREST_EXEC','504','223','224','2','0','1','1','0',0),
 ('TP_REG','204','9','11','3','0','1','3','0',1),
 ('TP_REG','501','9','11','3','0','1','3','0',1),
 ('TP_REG','502','9','11','3','0','1','3','0',1),
 ('TP_REG','503','9','11','3','0','1','3','0',1),
 ('TP_REG','504','9','11','3','0','1','3','0',1),
 ('TP_REG','505','9','11','3','0','1','3','0',1),
 ('TP_REG','509','9','11','3','0','1','1','0',1),
 ('TP_REG_CPL','505','31','31','1','0','1','1','0',0),
 ('TP_SAIDA','503','117','118','2','0','1','3','0',0),
 ('TP_SAIDA_CONS_SADT','502','123','123','1','0','1','3','0',0),
 ('TP_TABELA','504','92','92','1','0','1','1','0',0),
 ('VL_ADIC_CO','504','193','206','12','2','1','1','1',0),
 ('VL_ADIC_FILME','504','207','220','12','2','1','1','1',0),
 ('VL_ADIC_SER','504','179','192','12','2','1','1','1',0),
 ('VL_CO_COB','504','123','136','12','2','1','1','1',0),
 ('VL_CO_PAGO','204','130','143','12','2','1','1','1',0),
 ('VL_FILME_COB','504','137','150','12','2','1','1','1',0),
 ('VL_FILME_PAGO','204','144','157','12','2','1','1','1',0),
 ('VL_SERVICO_PAGO','204','116','129','12','2','1','1','1',0),
 ('VL_SERV_COB','504','109','122','12','2','1','1','1',0),
 ('VL_TOT_SERV','509','53','66','12','2','1','1','0',0);

--
-- Definition of table `db_rel`.`mascara_de_registro`
--

DROP TABLE IF EXISTS `db_rel`.`mascara_de_registro`;
CREATE TABLE  `db_rel`.`mascara_de_registro` (
  `cod_mascara_de_registro` varchar(6) NOT NULL,
  `dsc_mascara_de_registro` varchar(45) NOT NULL,
  `vlr_tamanho` decimal(4,0) NOT NULL,
  PRIMARY KEY (`cod_mascara_de_registro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_rel`.`mascara_de_registro`
--
INSERT INTO `db_rel`.`mascara_de_registro` VALUES  ('204','R204 - COMPLEMENTAR DE SERVICOS','0'),
 ('501','R501 - HEADER','84'),
 ('502','R502 - NOTA DE COBRANCA','108'),
 ('503','R503 - HOSPITALAR','363'),
 ('504','R504 - SERVICO','0'),
 ('505','R505 - COMPLEMENTAR','0'),
 ('509','R509 - TRAILER','59');

--
-- Definition of table `db_rel`.`tipo_de_campo`
--

DROP TABLE IF EXISTS `db_rel`.`tipo_de_campo`;
CREATE TABLE  `db_rel`.`tipo_de_campo` (
  `cod_tipo_de_campo` decimal(2,0) NOT NULL DEFAULT '0',
  `dsc_tipo_de_campo` varchar(45) NOT NULL,
  `cod_tipo_de_campo_no_destino` varchar(25) NOT NULL,
  `dsc_formato_tipo_de_campo` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`cod_tipo_de_campo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_rel`.`tipo_de_campo`
--
INSERT INTO `db_rel`.`tipo_de_campo` VALUES  ('0','ALFABETICO','VARCHAR',NULL),
 ('1','NUMERICO NAO SINALIZADO','DECIMAL',NULL),
 ('2','NUMERICO SINALIZADO','DECIMAL',NULL),
 ('3','ALFANUMERICO','VARCHAR',NULL),
 ('4','ALFANUMERICO COM CARACTERS ESPECIAIS','VARCHAR',NULL),
 ('5','MES DE 1 A 12','DECIMAL','NN'),
 ('6','DIA DE 1 A 31','DECIMAL','NN'),
 ('7','ANO DE 1900 a 2999','DECIMAL','NNNN'),
 ('8','HORA LOCAL DE 00 A 23','DECIMAL','NN'),
 ('9','Minutos Local de 00 a 59','DECIMAL','99'),
 ('10','Segundos Local de 00 a 59','DECIMAL','99'),
 ('11','DATA COM HORA MINUTO E SEGUNDO','DATETIME','YYYY/MM/DDHH:MM:SS'),
 ('12','DATA ','DATE','YYYYMMDD'),
 ('13','CID','VARCHAR','ANNC'),
 ('14','HORA','TIME','HH:MM:SS'),
 ('15','UF','CHAR',NULL),
 ('90','MINUTOS LOCAL DE 00 A 59','DECIMAL','99'),
 ('91','SEGUNDOS LOCAL DE 00 A 59','DECIMAL','99');

--
-- Definition of table `db_rel`.`tipo_de_categoria`
--

DROP TABLE IF EXISTS `db_rel`.`tipo_de_categoria`;
CREATE TABLE  `db_rel`.`tipo_de_categoria` (
  `cod_tipo_de_categoria` decimal(1,0) NOT NULL,
  `dsc_tipo_de_categoria` varchar(45) NOT NULL,
  PRIMARY KEY (`cod_tipo_de_categoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_rel`.`tipo_de_categoria`
--
INSERT INTO `db_rel`.`tipo_de_categoria` VALUES  ('0','ARQUIVO'),
 ('1','TABELA');

--
-- Definition of table `db_rel`.`tipo_de_origem`
--

DROP TABLE IF EXISTS `db_rel`.`tipo_de_origem`;
CREATE TABLE  `db_rel`.`tipo_de_origem` (
  `cod_tipo_de_origem` decimal(1,0) NOT NULL,
  `dsc_tipo_de_origem` varchar(45) NOT NULL,
  PRIMARY KEY (`cod_tipo_de_origem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_rel`.`tipo_de_origem`
--
INSERT INTO `db_rel`.`tipo_de_origem` VALUES  ('0','PARAMETRO'),
 ('1','REGISTRO');

--
-- Definition of table `db_rel`.`tipo_de_uso`
--

DROP TABLE IF EXISTS `db_rel`.`tipo_de_uso`;
CREATE TABLE  `db_rel`.`tipo_de_uso` (
  `cod_tipo_de_uso` decimal(1,0) NOT NULL,
  `dsc_tipo_de_uso` varchar(45) NOT NULL,
  PRIMARY KEY (`cod_tipo_de_uso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `db_rel`.`tipo_de_uso`
--
INSERT INTO `db_rel`.`tipo_de_uso` VALUES  ('0','MANDATORIO'),
 ('1','OPCIONAL');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
