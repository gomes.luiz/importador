package br.com.unimed.importador.dominio;

import org.junit.Ignore;
import java.util.Vector;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import br.com.unimed.importador.persistencia.dao.DataAccessFactory;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;

public class ControladorDeImportacaoTest {

    DataAccessFactory MySqlFactory;
    MascaraDeArquivo    arquivo1;
    MascaraDeRegistro   registro1;
    MascaraDeCampo      campo1;
    MascaraDeCampo      campo2;
    MascaraDeCampo      campo3;
    MascaraDeCampo      campo4;
    MascaraDeCampo      campo5;
    MascaraDeCampo      campo6;
    ControladorDeImportacao controlador;


    @Before
    public void setUp() {

        controlador = ControladorDeImportacao.getInstance();

        arquivo1    = new MascaraDeArquivo("A500", 0, "Arquivo de Notas");
        registro1   = new MascaraDeRegistro("502", 0, "Notas Fiscais");
/*
        campo1      = new MascaraDeCampo("FONTE",0, 0, 1, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, 0,0);
        campo2      = new MascaraDeCampo("REFERENCIA",0, 0, 6, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, 0,0);
        campo3      = new MascaraDeCampo("NR_SEQ",0, 0, 8, 0, TipoDeOrigem.REGISTRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, 0,0);
        campo4      = new MascaraDeCampo("TP_REG",0, 0, 3, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, false, 0,0);
        campo5      = new MascaraDeCampo("NR_LOTE",0, 0, 8, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, true, 0,0);
        campo6      = new MascaraDeCampo("NR_NOTA",0, 0, 11, 0, TipoDeOrigem.PARAMETRO, TipoDeCampo.A, TipoDeUso.MANDATORIO, false, true, 0,0);

        registro1.adicionaCampo(campo1);
        registro1.adicionaCampo(campo2);
        registro1.adicionaCampo(campo3);
        registro1.adicionaCampo(campo4);
        registro1.adicionaCampo(campo5);
        registro1.adicionaCampo(campo6);
*/
        arquivo1.adicionaMascaraDeRegistro(registro1);
    }

    @After
    public void tearDown() {
        arquivo1 = null;
        registro1 = null;
        campo1 = null;
        campo2 = null;
        campo3 = null;
        campo4 = null;
        campo5 = null;
        campo6 = null;
    }

     @Ignore
    public void testImportarArquivo() {
        System.out.println("ImportarArquivo");
/*
        String nomeDoArquivoDeOrigem = "/home/luiz/workspace/tests/util201010.217";
        String tipoDaMascaraDoArquivoDeOrigem = "A500";
        String nomeDoArquivoDeDestino = "";
        String tipoDaMascaraDoArquivoDeDestino = "";

        Map<String, MascaraDeCampo> parametrosDoUsuario = controlador.obterParametrosDoUsuario(tipoDaMascaraDoArquivoDeOrigem, 15);

        parametrosDoUsuario.get("502REFERENCIA").setValor("201010");
        parametrosDoUsuario.get("502FONTE").setValor("T");

        boolean apagarArquivoNoDestino = false;

        //controlador.ImportarArquivo(nomeDoArquivoDeOrigem, tipoDaMascaraDoArquivoDeOrigem, nomeDoArquivoDeDestino, tipoDaMascaraDoArquivoDeDestino, parametrosDoUsuario, apagarArquivoNoDestino);
*/
    }
     
    @Ignore
    public void testGetInstance() {
        System.out.println("getInstance");

        assertEquals("getInstance", controlador, ControladorDeImportacao.getInstance());
    }

    @Ignore
    public void testObterTiposDeArquivo() {
        List<String> expResult;
        List result;

        expResult = new ArrayList<String>();
        expResult.add("A500");
        expResult.add("A200");

        result = ControladorDeImportacao.getInstance().obterTiposDeArquivo(15);

        assertEquals("obterTiposDeArquivos: ", expResult, result);
    }

    @Test
    public void testObterParametrosDoUsuario() {
        System.out.println("INICIO: obterParametrosDoUsuario");

        ControladorDeImportacao instance = ControladorDeImportacao.getInstance();

        String mascaraDeArquivo = "A500";
    
        Map<String, MascaraDeCampo> hmExpResult = new HashMap<String, MascaraDeCampo>();
//        hmExpResult.put("502" + campo1.getCodigo(), campo1);
//        hmExpResult.put("502" + campo2.getCodigo(), campo2);

//        Map<String, MascaraDeCampo> hmResult = instance.obterParametrosDoUsuario(mascaraDeArquivo, 15);
        System.out.println("FIM: obterParametrosDoUsuario");
 //       Set<String> chResult = hmResult.keySet();
 //       Set<String> chExpResult = hmExpResult.keySet();


//        assertEquals(chExpResult, chResult);

    }
   
    @Ignore
    public void testObterChavePrimariaFormatada(){
       /*
        final Iterator<MascaraDeCampo> campos = registro1.getCampos().iterator();
        final String resultadoEsperado = 
                "CONSTRAINT pk_R502 PRIMARY KEY(NR_LOTE,NR_NOTA)";
        final String resultado =
                controlador.obterChavePrimariaFormatada("R502", campos);

        assertEquals("testObterChavePrimariaFormatada", resultadoEsperado, resultado);
        * 
        */
    }

   @Test
   public void testFormatarPendenciasDeAlteracaoSemPendencia() {
     // final String resultado = controlador.formatarPendenciasDeAlteracao("R502", registro1);
     // assertTrue("testFormatarPendenciasDeAlteracaoSemPendencia : ", resultado.length() == 0);
   }
/*
   @Test
   public void testFormatarPendenciasDeAlteracaoComPendencia() {
      PendenciaDeAlteracao pendencia1 = new PendenciaDeAlteracao(3,"I", false);
      PendenciaDeAlteracao pendencia2 = new PendenciaDeAlteracao(2,"A", false);
      PendenciaDeAlteracao pendencia3 = new PendenciaDeAlteracao(1,"E", false);
      campo1.adicionarPendencia(pendencia3);
      campo1.adicionarPendencia(pendencia1);
      campo1.adicionarPendencia(pendencia2);
      

      final String resultado = controlador.formatarPendenciasDeAlteracao("R502", registro1);

System.out.println("resultado : "+ resultado);
      assertTrue("testFormatarPendenciasDeAlteracaoComPendencia : ", resultado.length() != 0);
   }
*/
     @Ignore
    public void testObterTabelaFormatada(){
        // final Vector<MascaraDeCampo> campos = registro1.getCampos();
        final String resultadoEsperado =
                "CONSTRAINT pk_R502 PRIMARY KEY(NR_LOTE,NR_NOTA)";
        // final String resultado =
        //        controlador.formatarInstrucaoCriacaoTabela("R502", campos);

        //assertEquals("testObterTabelaFormatada", resultadoEsperado, resultado);
    }
  
}
