package br.com.unimed.importador.exceptions;

public class RegistroComTamanhoMenorException extends RegistroComTamanhoInvalidoException {

    public RegistroComTamanhoMenorException() {
       super("Registro com tamanho menor do que esperado");
    }
    public RegistroComTamanhoMenorException(String msg) {
        super(msg);
    }
    public RegistroComTamanhoMenorException(final String msg, final Throwable causa) {
        super(msg, causa);
    }
}
