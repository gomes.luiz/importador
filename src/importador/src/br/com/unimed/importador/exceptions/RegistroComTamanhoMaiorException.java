package br.com.unimed.importador.exceptions;

public class RegistroComTamanhoMaiorException extends RegistroComTamanhoInvalidoException {

    public RegistroComTamanhoMaiorException() {
       super("Registro com o tamanho maior do que esperado");
    }
    public RegistroComTamanhoMaiorException(String msg) {
        super(msg);
    }
    public RegistroComTamanhoMaiorException(final String msg, final Throwable causa) {
        super(msg, causa);
    }
}
