/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.PreexistingEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeCampo;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeCampoPK;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeUso;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeOrigem;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeCampo;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeRegistro;

/**
 *
 * @author paulo
 */
public class MascaraDeCampoJpaController {

    public MascaraDeCampoJpaController() {
        emf = Persistence.createEntityManagerFactory("Importador-CadPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MascaraDeCampo mascaraDeCampo) throws PreexistingEntityException, Exception {
        if (mascaraDeCampo.getMascaraDeCampoPK() == null) {
            mascaraDeCampo.setMascaraDeCampoPK(new MascaraDeCampoPK());
        }
        mascaraDeCampo.getMascaraDeCampoPK().setNumVersaoMascaraDeRegistro(mascaraDeCampo.getMascaraDeRegistro().getMascaraDeRegistroPK().getNumVersaoMascaraDeRegistro());
        mascaraDeCampo.getMascaraDeCampoPK().setCodMascaraDeRegistro(mascaraDeCampo.getMascaraDeRegistro().getMascaraDeRegistroPK().getCodMascaraDeRegistro());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoDeUso tipoDeUso = mascaraDeCampo.getTipoDeUso();
            if (tipoDeUso != null) {
                tipoDeUso = em.getReference(tipoDeUso.getClass(), tipoDeUso.getCodTipoDeUso());
                mascaraDeCampo.setTipoDeUso(tipoDeUso);
            }
            TipoDeOrigem tipoDeOrigem = mascaraDeCampo.getTipoDeOrigem();
            if (tipoDeOrigem != null) {
                tipoDeOrigem = em.getReference(tipoDeOrigem.getClass(), tipoDeOrigem.getCodTipoDeOrigem());
                mascaraDeCampo.setTipoDeOrigem(tipoDeOrigem);
            }
            TipoDeCampo tipoDeCampo = mascaraDeCampo.getTipoDeCampo();
            if (tipoDeCampo != null) {
                tipoDeCampo = em.getReference(tipoDeCampo.getClass(), tipoDeCampo.getCodTipoDeCampo());
                mascaraDeCampo.setTipoDeCampo(tipoDeCampo);
            }
            MascaraDeRegistro mascaraDeRegistro = mascaraDeCampo.getMascaraDeRegistro();
            if (mascaraDeRegistro != null) {
                mascaraDeRegistro = em.getReference(mascaraDeRegistro.getClass(), mascaraDeRegistro.getMascaraDeRegistroPK());
                mascaraDeCampo.setMascaraDeRegistro(mascaraDeRegistro);
            }
            em.persist(mascaraDeCampo);
            if (tipoDeUso != null) {
                tipoDeUso.getMascaraDeCampoCollection().add(mascaraDeCampo);
                tipoDeUso = em.merge(tipoDeUso);
            }
            if (tipoDeOrigem != null) {
                tipoDeOrigem.getMascaraDeCampoCollection().add(mascaraDeCampo);
                tipoDeOrigem = em.merge(tipoDeOrigem);
            }
            if (tipoDeCampo != null) {
                tipoDeCampo.getMascaraDeCampoCollection().add(mascaraDeCampo);
                tipoDeCampo = em.merge(tipoDeCampo);
            }
            if (mascaraDeRegistro != null) {
                mascaraDeRegistro.getMascaraDeCampoCollection().add(mascaraDeCampo);
                mascaraDeRegistro = em.merge(mascaraDeRegistro);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMascaraDeCampo(mascaraDeCampo.getMascaraDeCampoPK()) != null) {
                throw new PreexistingEntityException("MascaraDeCampo " + mascaraDeCampo + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MascaraDeCampo mascaraDeCampo) throws NonexistentEntityException, Exception {
        mascaraDeCampo.getMascaraDeCampoPK().setNumVersaoMascaraDeRegistro(mascaraDeCampo.getMascaraDeRegistro().getMascaraDeRegistroPK().getNumVersaoMascaraDeRegistro());
        mascaraDeCampo.getMascaraDeCampoPK().setCodMascaraDeRegistro(mascaraDeCampo.getMascaraDeRegistro().getMascaraDeRegistroPK().getCodMascaraDeRegistro());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MascaraDeCampo persistentMascaraDeCampo = em.find(MascaraDeCampo.class, mascaraDeCampo.getMascaraDeCampoPK());
            TipoDeUso tipoDeUsoOld = persistentMascaraDeCampo.getTipoDeUso();
            TipoDeUso tipoDeUsoNew = mascaraDeCampo.getTipoDeUso();
            TipoDeOrigem tipoDeOrigemOld = persistentMascaraDeCampo.getTipoDeOrigem();
            TipoDeOrigem tipoDeOrigemNew = mascaraDeCampo.getTipoDeOrigem();
            TipoDeCampo tipoDeCampoOld = persistentMascaraDeCampo.getTipoDeCampo();
            TipoDeCampo tipoDeCampoNew = mascaraDeCampo.getTipoDeCampo();
            MascaraDeRegistro mascaraDeRegistroOld = persistentMascaraDeCampo.getMascaraDeRegistro();
            MascaraDeRegistro mascaraDeRegistroNew = mascaraDeCampo.getMascaraDeRegistro();
            if (tipoDeUsoNew != null) {
                tipoDeUsoNew = em.getReference(tipoDeUsoNew.getClass(), tipoDeUsoNew.getCodTipoDeUso());
                mascaraDeCampo.setTipoDeUso(tipoDeUsoNew);
            }
            if (tipoDeOrigemNew != null) {
                tipoDeOrigemNew = em.getReference(tipoDeOrigemNew.getClass(), tipoDeOrigemNew.getCodTipoDeOrigem());
                mascaraDeCampo.setTipoDeOrigem(tipoDeOrigemNew);
            }
            if (tipoDeCampoNew != null) {
                tipoDeCampoNew = em.getReference(tipoDeCampoNew.getClass(), tipoDeCampoNew.getCodTipoDeCampo());
                mascaraDeCampo.setTipoDeCampo(tipoDeCampoNew);
            }
            if (mascaraDeRegistroNew != null) {
                mascaraDeRegistroNew = em.getReference(mascaraDeRegistroNew.getClass(), mascaraDeRegistroNew.getMascaraDeRegistroPK());
                mascaraDeCampo.setMascaraDeRegistro(mascaraDeRegistroNew);
            }
            mascaraDeCampo = em.merge(mascaraDeCampo);
            if (tipoDeUsoOld != null && !tipoDeUsoOld.equals(tipoDeUsoNew)) {
                tipoDeUsoOld.getMascaraDeCampoCollection().remove(mascaraDeCampo);
                tipoDeUsoOld = em.merge(tipoDeUsoOld);
            }
            if (tipoDeUsoNew != null && !tipoDeUsoNew.equals(tipoDeUsoOld)) {
                tipoDeUsoNew.getMascaraDeCampoCollection().add(mascaraDeCampo);
                tipoDeUsoNew = em.merge(tipoDeUsoNew);
            }
            if (tipoDeOrigemOld != null && !tipoDeOrigemOld.equals(tipoDeOrigemNew)) {
                tipoDeOrigemOld.getMascaraDeCampoCollection().remove(mascaraDeCampo);
                tipoDeOrigemOld = em.merge(tipoDeOrigemOld);
            }
            if (tipoDeOrigemNew != null && !tipoDeOrigemNew.equals(tipoDeOrigemOld)) {
                tipoDeOrigemNew.getMascaraDeCampoCollection().add(mascaraDeCampo);
                tipoDeOrigemNew = em.merge(tipoDeOrigemNew);
            }
            if (tipoDeCampoOld != null && !tipoDeCampoOld.equals(tipoDeCampoNew)) {
                tipoDeCampoOld.getMascaraDeCampoCollection().remove(mascaraDeCampo);
                tipoDeCampoOld = em.merge(tipoDeCampoOld);
            }
            if (tipoDeCampoNew != null && !tipoDeCampoNew.equals(tipoDeCampoOld)) {
                tipoDeCampoNew.getMascaraDeCampoCollection().add(mascaraDeCampo);
                tipoDeCampoNew = em.merge(tipoDeCampoNew);
            }
            if (mascaraDeRegistroOld != null && !mascaraDeRegistroOld.equals(mascaraDeRegistroNew)) {
                mascaraDeRegistroOld.getMascaraDeCampoCollection().remove(mascaraDeCampo);
                mascaraDeRegistroOld = em.merge(mascaraDeRegistroOld);
            }
            if (mascaraDeRegistroNew != null && !mascaraDeRegistroNew.equals(mascaraDeRegistroOld)) {
                mascaraDeRegistroNew.getMascaraDeCampoCollection().add(mascaraDeCampo);
                mascaraDeRegistroNew = em.merge(mascaraDeRegistroNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                MascaraDeCampoPK id = mascaraDeCampo.getMascaraDeCampoPK();
                if (findMascaraDeCampo(id) == null) {
                    throw new NonexistentEntityException("The mascaraDeCampo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(MascaraDeCampoPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MascaraDeCampo mascaraDeCampo;
            try {
                mascaraDeCampo = em.getReference(MascaraDeCampo.class, id);
                mascaraDeCampo.getMascaraDeCampoPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mascaraDeCampo with id " + id + " no longer exists.", enfe);
            }
            TipoDeUso tipoDeUso = mascaraDeCampo.getTipoDeUso();
            if (tipoDeUso != null) {
                tipoDeUso.getMascaraDeCampoCollection().remove(mascaraDeCampo);
                tipoDeUso = em.merge(tipoDeUso);
            }
            TipoDeOrigem tipoDeOrigem = mascaraDeCampo.getTipoDeOrigem();
            if (tipoDeOrigem != null) {
                tipoDeOrigem.getMascaraDeCampoCollection().remove(mascaraDeCampo);
                tipoDeOrigem = em.merge(tipoDeOrigem);
            }
            TipoDeCampo tipoDeCampo = mascaraDeCampo.getTipoDeCampo();
            if (tipoDeCampo != null) {
                tipoDeCampo.getMascaraDeCampoCollection().remove(mascaraDeCampo);
                tipoDeCampo = em.merge(tipoDeCampo);
            }
            MascaraDeRegistro mascaraDeRegistro = mascaraDeCampo.getMascaraDeRegistro();
            if (mascaraDeRegistro != null) {
                mascaraDeRegistro.getMascaraDeCampoCollection().remove(mascaraDeCampo);
                mascaraDeRegistro = em.merge(mascaraDeRegistro);
            }
            em.remove(mascaraDeCampo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MascaraDeCampo> findMascaraDeCampoEntities() {
        return findMascaraDeCampoEntities(true, -1, -1);
    }

    public List<MascaraDeCampo> findMascaraDeCampoEntities(int maxResults, int firstResult) {
        return findMascaraDeCampoEntities(false, maxResults, firstResult);
    }

    private List<MascaraDeCampo> findMascaraDeCampoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MascaraDeCampo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MascaraDeCampo findMascaraDeCampo(MascaraDeCampoPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MascaraDeCampo.class, id);
        } finally {
            em.close();
        }
    }

    public int getMascaraDeCampoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MascaraDeCampo> rt = cq.from(MascaraDeCampo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
