/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author paulo
 */
@Entity
@Table(name = "tipo_de_uso")
@NamedQueries({
    @NamedQuery(name = "TipoDeUso.findAll", query = "SELECT t FROM TipoDeUso t"),
    @NamedQuery(name = "TipoDeUso.findByCodTipoDeUso", query = "SELECT t FROM TipoDeUso t WHERE t.codTipoDeUso = :codTipoDeUso"),
    @NamedQuery(name = "TipoDeUso.findByDscTipoDeUso", query = "SELECT t FROM TipoDeUso t WHERE t.dscTipoDeUso = :dscTipoDeUso")})
public class TipoDeUso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cod_tipo_de_uso")
    private Boolean codTipoDeUso;
    @Basic(optional = false)
    @Column(name = "dsc_tipo_de_uso")
    private String dscTipoDeUso;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoDeUso")
    private Collection<MascaraDeCampo> mascaraDeCampoCollection;

    public TipoDeUso() {
    }

    public TipoDeUso(Boolean codTipoDeUso) {
        this.codTipoDeUso = codTipoDeUso;
    }

    public TipoDeUso(Boolean codTipoDeUso, String dscTipoDeUso) {
        this.codTipoDeUso = codTipoDeUso;
        this.dscTipoDeUso = dscTipoDeUso;
    }

    public Boolean getCodTipoDeUso() {
        return codTipoDeUso;
    }

    public void setCodTipoDeUso(Boolean codTipoDeUso) {
        this.codTipoDeUso = codTipoDeUso;
    }

    public String getDscTipoDeUso() {
        return dscTipoDeUso;
    }

    public void setDscTipoDeUso(String dscTipoDeUso) {
        this.dscTipoDeUso = dscTipoDeUso;
    }

    public Collection<MascaraDeCampo> getMascaraDeCampoCollection() {
        return mascaraDeCampoCollection;
    }

    public void setMascaraDeCampoCollection(Collection<MascaraDeCampo> mascaraDeCampoCollection) {
        this.mascaraDeCampoCollection = mascaraDeCampoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoDeUso != null ? codTipoDeUso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDeUso)) {
            return false;
        }
        TipoDeUso other = (TipoDeUso) object;
        if ((this.codTipoDeUso == null && other.codTipoDeUso != null) || (this.codTipoDeUso != null && !this.codTipoDeUso.equals(other.codTipoDeUso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDeUso[codTipoDeUso=" + codTipoDeUso + "]";
    }

}
