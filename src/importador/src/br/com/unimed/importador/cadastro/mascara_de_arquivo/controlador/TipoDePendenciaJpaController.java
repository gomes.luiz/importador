/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.IllegalOrphanException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.PreexistingEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.TipoDePendencia;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.PendenciaDeAlteracao;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author paulo
 */
public class TipoDePendenciaJpaController {

    public TipoDePendenciaJpaController() {
        emf = Persistence.createEntityManagerFactory("Importador-CadPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoDePendencia tipoDePendencia) throws PreexistingEntityException, Exception {
        if (tipoDePendencia.getPendenciaDeAlteracaoCollection() == null) {
            tipoDePendencia.setPendenciaDeAlteracaoCollection(new ArrayList<PendenciaDeAlteracao>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<PendenciaDeAlteracao> attachedPendenciaDeAlteracaoCollection = new ArrayList<PendenciaDeAlteracao>();
            for (PendenciaDeAlteracao pendenciaDeAlteracaoCollectionPendenciaDeAlteracaoToAttach : tipoDePendencia.getPendenciaDeAlteracaoCollection()) {
                pendenciaDeAlteracaoCollectionPendenciaDeAlteracaoToAttach = em.getReference(pendenciaDeAlteracaoCollectionPendenciaDeAlteracaoToAttach.getClass(), pendenciaDeAlteracaoCollectionPendenciaDeAlteracaoToAttach.getNumSequencialPendencia());
                attachedPendenciaDeAlteracaoCollection.add(pendenciaDeAlteracaoCollectionPendenciaDeAlteracaoToAttach);
            }
            tipoDePendencia.setPendenciaDeAlteracaoCollection(attachedPendenciaDeAlteracaoCollection);
            em.persist(tipoDePendencia);
            for (PendenciaDeAlteracao pendenciaDeAlteracaoCollectionPendenciaDeAlteracao : tipoDePendencia.getPendenciaDeAlteracaoCollection()) {
                TipoDePendencia oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionPendenciaDeAlteracao = pendenciaDeAlteracaoCollectionPendenciaDeAlteracao.getTipoDePendencia();
                pendenciaDeAlteracaoCollectionPendenciaDeAlteracao.setTipoDePendencia(tipoDePendencia);
                pendenciaDeAlteracaoCollectionPendenciaDeAlteracao = em.merge(pendenciaDeAlteracaoCollectionPendenciaDeAlteracao);
                if (oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionPendenciaDeAlteracao != null) {
                    oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionPendenciaDeAlteracao.getPendenciaDeAlteracaoCollection().remove(pendenciaDeAlteracaoCollectionPendenciaDeAlteracao);
                    oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionPendenciaDeAlteracao = em.merge(oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionPendenciaDeAlteracao);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTipoDePendencia(tipoDePendencia.getCodTipoDePendencia()) != null) {
                throw new PreexistingEntityException("TipoDePendencia " + tipoDePendencia + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoDePendencia tipoDePendencia) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoDePendencia persistentTipoDePendencia = em.find(TipoDePendencia.class, tipoDePendencia.getCodTipoDePendencia());
            Collection<PendenciaDeAlteracao> pendenciaDeAlteracaoCollectionOld = persistentTipoDePendencia.getPendenciaDeAlteracaoCollection();
            Collection<PendenciaDeAlteracao> pendenciaDeAlteracaoCollectionNew = tipoDePendencia.getPendenciaDeAlteracaoCollection();
            List<String> illegalOrphanMessages = null;
            for (PendenciaDeAlteracao pendenciaDeAlteracaoCollectionOldPendenciaDeAlteracao : pendenciaDeAlteracaoCollectionOld) {
                if (!pendenciaDeAlteracaoCollectionNew.contains(pendenciaDeAlteracaoCollectionOldPendenciaDeAlteracao)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PendenciaDeAlteracao " + pendenciaDeAlteracaoCollectionOldPendenciaDeAlteracao + " since its tipoDePendencia field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<PendenciaDeAlteracao> attachedPendenciaDeAlteracaoCollectionNew = new ArrayList<PendenciaDeAlteracao>();
            for (PendenciaDeAlteracao pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracaoToAttach : pendenciaDeAlteracaoCollectionNew) {
                pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracaoToAttach = em.getReference(pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracaoToAttach.getClass(), pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracaoToAttach.getNumSequencialPendencia());
                attachedPendenciaDeAlteracaoCollectionNew.add(pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracaoToAttach);
            }
            pendenciaDeAlteracaoCollectionNew = attachedPendenciaDeAlteracaoCollectionNew;
            tipoDePendencia.setPendenciaDeAlteracaoCollection(pendenciaDeAlteracaoCollectionNew);
            tipoDePendencia = em.merge(tipoDePendencia);
            for (PendenciaDeAlteracao pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao : pendenciaDeAlteracaoCollectionNew) {
                if (!pendenciaDeAlteracaoCollectionOld.contains(pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao)) {
                    TipoDePendencia oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao = pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao.getTipoDePendencia();
                    pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao.setTipoDePendencia(tipoDePendencia);
                    pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao = em.merge(pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao);
                    if (oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao != null && !oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao.equals(tipoDePendencia)) {
                        oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao.getPendenciaDeAlteracaoCollection().remove(pendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao);
                        oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao = em.merge(oldTipoDePendenciaOfPendenciaDeAlteracaoCollectionNewPendenciaDeAlteracao);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Short id = tipoDePendencia.getCodTipoDePendencia();
                if (findTipoDePendencia(id) == null) {
                    throw new NonexistentEntityException("The tipoDePendencia with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoDePendencia tipoDePendencia;
            try {
                tipoDePendencia = em.getReference(TipoDePendencia.class, id);
                tipoDePendencia.getCodTipoDePendencia();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoDePendencia with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<PendenciaDeAlteracao> pendenciaDeAlteracaoCollectionOrphanCheck = tipoDePendencia.getPendenciaDeAlteracaoCollection();
            for (PendenciaDeAlteracao pendenciaDeAlteracaoCollectionOrphanCheckPendenciaDeAlteracao : pendenciaDeAlteracaoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoDePendencia (" + tipoDePendencia + ") cannot be destroyed since the PendenciaDeAlteracao " + pendenciaDeAlteracaoCollectionOrphanCheckPendenciaDeAlteracao + " in its pendenciaDeAlteracaoCollection field has a non-nullable tipoDePendencia field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoDePendencia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoDePendencia> findTipoDePendenciaEntities() {
        return findTipoDePendenciaEntities(true, -1, -1);
    }

    public List<TipoDePendencia> findTipoDePendenciaEntities(int maxResults, int firstResult) {
        return findTipoDePendenciaEntities(false, maxResults, firstResult);
    }

    private List<TipoDePendencia> findTipoDePendenciaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoDePendencia.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoDePendencia findTipoDePendencia(Short id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoDePendencia.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoDePendenciaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoDePendencia> rt = cq.from(TipoDePendencia.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
