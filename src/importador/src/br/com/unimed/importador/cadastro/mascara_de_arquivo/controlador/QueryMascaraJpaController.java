/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador;


import br.com.unimed.importador.cadastro.mascara_de_arquivo.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.QueryMascara;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.visao.MyTableModel;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.swing.JTable;

/**
 * Classe responsável em recuperar e gerenciar as informações do cadastro de
 * mascara de arquivo e mascara de registro
 * @author Equipe Affirmare
 */
public class QueryMascaraJpaController {

    public QueryMascaraJpaController() {
        emf = Persistence.createEntityManagerFactory("Importador-CadPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(QueryMascara queryMascara) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(queryMascara);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(QueryMascara queryMascara) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            queryMascara = em.merge(queryMascara);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = queryMascara.getId();
                if (findQueryMascara(id) == null) {
                    throw new NonexistentEntityException("The queryMascara with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            QueryMascara queryMascara;
            try {
                queryMascara = em.getReference(QueryMascara.class, id);
                queryMascara.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The queryMascara with id " + id + " no longer exists.", enfe);
            }
            em.remove(queryMascara);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<QueryMascara> findQueryMascaraEntities() {
        return findQueryMascaraEntities(true, -1, -1);
    }

    public List<QueryMascara> findQueryMascaraEntities(int maxResults, int firstResult) {
        return findQueryMascaraEntities(false, maxResults, firstResult);
    }

    private List<QueryMascara> findQueryMascaraEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(QueryMascara.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public QueryMascara findQueryMascara(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(QueryMascara.class, id);
        } finally {
            em.close();
        }
    }

    public int getQueryMascaraCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<QueryMascara> rt = cq.from(QueryMascara.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    
    /**
     * Inserir informações na tabela. O método recebe a JTable por parametro
     * e atualiza as informações.
     * @param tblManutencao Tabela que receberá as informações 
     * @param modeloTabela Modelo da tabela
     * @param codMascaraDeArquivo filtro dos elementos da tabela
     */
    public void preencheTabelaMascaraDeArquivo(JTable tblManutencao, MyTableModel modeloTabela,
            String codMascaraDeArquivo,short numVersao) {

        String sql = "SELECT mr.num_sequencial_registro, r.cod_mascara_de_registro,r.dsc_mascara_de_registro"
                + " FROM mascara_de_arquivo m,"
                + " mascara_de_registro r, "
                + " mascara_de_arquivo_mascara_de_registro mr "
                + " where mr.cod_mascara_de_arquivo=m.cod_mascara_de_arquivo "
                + " and mr.cod_mascara_de_registro=r.cod_mascara_de_registro "
                + " and m.cod_mascara_de_arquivo= ?1"
                + " and mr.num_versao_mascara_de_arquivo=?2 and m.num_versao_mascara_de_arquivo=?2 "
                + "and r.num_versao_mascara_de_registro=?2"
                + " order by mr.num_sequencial_registro";

        System.out.println(sql);

        javax.persistence.Query query = getEntityManager().
                createNativeQuery(sql);

        query.setParameter(1, codMascaraDeArquivo);
        query.setParameter(2, numVersao);

        List<Object[]> results = (List<Object[]>) query.getResultList();

        Object data[][] = new Object[results.size()][3];

        Object[] columns = new Object[3];
        columns[0] = "Sequencia";
        columns[1] = "Código";
        columns[2] = "Descrição da Mascara";

        int i = 0; //contador

        if (results != null && !results.isEmpty()) {

            Iterator it = results.iterator();

            while (it.hasNext()) {
                Object[] ob = (Object[]) it.next();
                data[i][0] = ob[0];
                data[i][1] = ob[1];
                data[i][2] = ob[2];
                i++;
            }

            modeloTabela.setRowData(data);
            modeloTabela.setColumnName(columns);
            modeloTabela.updateTable();
            tblManutencao.setModel(modeloTabela);
        } else {
            modeloTabela.setRowData(new Object[0][0]);
            modeloTabela.setColumnName(columns);
            modeloTabela.updateTable();
            tblManutencao.setModel(modeloTabela);
        }
    }

    /**
     * Inserir informações na tabela. O método recebe a JTable por parametro
     * e atualiza as informações.
     * @param tblManutencao JTable que receberá as informações
     * @param modeloTabela Modelo da tabela
     * @param codMascaraDeArquivo Filtro dos dados
     */
    public void preencheTabelaMascaraDeRegistro(JTable tblManutencao, MyTableModel modeloTabela,
            String codMascaraDeArquivo, short numVersao) {

        String sql = "SELECT m.num_sequencial_campo,m.cod_mascara_de_campo, tpo.dsc_tipo_de_origem, tc.dsc_tipo_de_campo,"
                + " m.vlr_posicao_inicial,m.vlr_posicao_final,m.vlr_tamanho,m.vlr_decimais,m.ind_chave_primaria,"
                + " m.cod_tipo_de_uso,m.ind_contem_tipo_de_registro  from mascara_de_campo m, tipo_de_uso tu, tipo_de_origem tpo, tipo_de_campo tc"
                + " where tu.cod_tipo_de_uso = m.cod_tipo_de_uso and tpo.cod_tipo_de_origem = m.cod_tipo_de_origem and "
                + " tc.cod_tipo_de_campo = m.cod_tipo_de_campo "
                + " and m.num_versao_mascara_de_registro=?2 and "
                + " m.cod_mascara_de_registro = ?1 order by num_sequencial_campo ";

        javax.persistence.Query query = getEntityManager().
                createNativeQuery(sql);

        query.setParameter(1, codMascaraDeArquivo);
        query.setParameter(2, numVersao);
        List<Object[]> results = (List<Object[]>) query.getResultList();
        Object data[][] = new Object[results.size()][11];

        Object[] columns = new Object[11];
        columns[0] = "Seq.";
        columns[1] = "Nome";
        columns[2] = "Origem";
        columns[3] = "Tipo";
        columns[4] = "Pos Ini";
        columns[5] = "Pos Fim";
        columns[6] = "Tamanho";
        columns[7] = "Decimais";
        columns[8] = "Ch.Primaria";
        columns[9] = "Mandatório";
        columns[10] = "Id. Registro";

        int i = 0; //contador

        if (results != null && !results.isEmpty()) {

            Iterator it = results.iterator();

            while (it.hasNext()) {
                Object[] ob = (Object[]) it.next();
                data[i][0] = ob[0];
                data[i][1] = ob[1];
                data[i][2] = ob[2];
                data[i][3] = ob[3];
                data[i][4] = ob[4];
                data[i][5] = ob[5];
                data[i][6] = ob[6];
                data[i][7] = ob[7];
                data[i][8] = ob[8];
                data[i][9] = ob[9];
                data[i][10] = ob[10];
                i++;
            }

            modeloTabela.setRowData(data);
            modeloTabela.setColumnName(columns);
            tblManutencao.setModel(modeloTabela);
            modeloTabela.updateTable();

        } else {
            modeloTabela.setRowData(new Object[0][0]);
            modeloTabela.setColumnName(columns);
            tblManutencao.setModel(modeloTabela);
            modeloTabela.updateTable();
        }
    }


}
