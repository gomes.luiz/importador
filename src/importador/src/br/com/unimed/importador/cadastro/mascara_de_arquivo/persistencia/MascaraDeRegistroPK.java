/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author paulo
 */
@Embeddable
public class MascaraDeRegistroPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "cod_mascara_de_registro")
    private String codMascaraDeRegistro;
    @Basic(optional = false)
    @Column(name = "num_versao_mascara_de_registro")
    private short numVersaoMascaraDeRegistro;

    public MascaraDeRegistroPK() {
    }

    public MascaraDeRegistroPK(String codMascaraDeRegistro, short numVersaoMascaraDeRegistro) {
        this.codMascaraDeRegistro = codMascaraDeRegistro;
        this.numVersaoMascaraDeRegistro = numVersaoMascaraDeRegistro;
    }

    public String getCodMascaraDeRegistro() {
        return codMascaraDeRegistro;
    }

    public void setCodMascaraDeRegistro(String codMascaraDeRegistro) {
        this.codMascaraDeRegistro = codMascaraDeRegistro;
    }

    public short getNumVersaoMascaraDeRegistro() {
        return numVersaoMascaraDeRegistro;
    }

    public void setNumVersaoMascaraDeRegistro(short numVersaoMascaraDeRegistro) {
        this.numVersaoMascaraDeRegistro = numVersaoMascaraDeRegistro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codMascaraDeRegistro != null ? codMascaraDeRegistro.hashCode() : 0);
        hash += (int) numVersaoMascaraDeRegistro;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MascaraDeRegistroPK)) {
            return false;
        }
        MascaraDeRegistroPK other = (MascaraDeRegistroPK) object;
        if ((this.codMascaraDeRegistro == null && other.codMascaraDeRegistro != null) || (this.codMascaraDeRegistro != null && !this.codMascaraDeRegistro.equals(other.codMascaraDeRegistro))) {
            return false;
        }
        if (this.numVersaoMascaraDeRegistro != other.numVersaoMascaraDeRegistro) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeRegistroPK[codMascaraDeRegistro=" + codMascaraDeRegistro + ", numVersaoMascaraDeRegistro=" + numVersaoMascaraDeRegistro + "]";
    }

}
