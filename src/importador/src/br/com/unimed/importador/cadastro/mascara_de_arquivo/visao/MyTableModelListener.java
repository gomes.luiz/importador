/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.cadastro.mascara_de_arquivo.visao;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

public class MyTableModelListener implements TableModelListener {

    private MyTableListenerEvent updateTable;
    private JTable table = null;

    /**
     * Informa a tabela ser monitorada
     * @param table JTable a ser monitorada
     */
    MyTableModelListener(JTable table) {
        this.table = table;
    }

    /**
     * Notifica alterações na tabela
     * @param e Evento que ocorreu. Por exemplo, Inserção, alteração e deleção
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE && e.getLastRow() != Integer.MAX_VALUE) {
            updateTable.notifyUpdateTable(e.getLastRow(), e.getColumn());
        } else if (e.getType() == TableModelEvent.INSERT) {
            updateTable.notifyInsertTable(e.getLastRow(), e.getColumn());
        } else if (e.getType() == TableModelEvent.DELETE) {
            updateTable.notifyDeleteTable(e.getLastRow(), e.getColumn());
        }
    }

    /**
     * Registra um listener para tabela
     * @param updateTable
     */
    public void addUpdateTableListener(MyTableListenerEvent updateTable) {
        this.updateTable = updateTable;
    }

    /**
     * remove um listener da tabela
     */
    public void removeUpdateTableListener() {
        this.updateTable = null;
    }
}
