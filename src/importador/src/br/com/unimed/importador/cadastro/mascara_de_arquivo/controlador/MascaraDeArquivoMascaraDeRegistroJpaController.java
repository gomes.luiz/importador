/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.PreexistingEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivoMascaraDeRegistro;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivoMascaraDeRegistroPK;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeRegistro;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivo;

/**
 *
 * @author paulo
 */
public class MascaraDeArquivoMascaraDeRegistroJpaController {

    public MascaraDeArquivoMascaraDeRegistroJpaController() {
        emf = Persistence.createEntityManagerFactory("Importador-CadPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistro) throws PreexistingEntityException, Exception {
        if (mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK() == null) {
            mascaraDeArquivoMascaraDeRegistro.setMascaraDeArquivoMascaraDeRegistroPK(new MascaraDeArquivoMascaraDeRegistroPK());
        }
        mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK().setNumVersaoMascaraDeArquivo(mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivo().getMascaraDeArquivoPK().getNumVersaoMascaraDeArquivo());
        mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK().setNumVersaoMascaraDeRegistro(mascaraDeArquivoMascaraDeRegistro.getMascaraDeRegistro().getMascaraDeRegistroPK().getNumVersaoMascaraDeRegistro());
        mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK().setCodMascaraDeRegistro(mascaraDeArquivoMascaraDeRegistro.getMascaraDeRegistro().getMascaraDeRegistroPK().getCodMascaraDeRegistro());
        mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK().setCodMascaraDeArquivo(mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivo().getMascaraDeArquivoPK().getCodMascaraDeArquivo());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MascaraDeRegistro mascaraDeRegistro = mascaraDeArquivoMascaraDeRegistro.getMascaraDeRegistro();
            if (mascaraDeRegistro != null) {
                mascaraDeRegistro = em.getReference(mascaraDeRegistro.getClass(), mascaraDeRegistro.getMascaraDeRegistroPK());
                mascaraDeArquivoMascaraDeRegistro.setMascaraDeRegistro(mascaraDeRegistro);
            }
            MascaraDeArquivo mascaraDeArquivo = mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivo();
            if (mascaraDeArquivo != null) {
                mascaraDeArquivo = em.getReference(mascaraDeArquivo.getClass(), mascaraDeArquivo.getMascaraDeArquivoPK());
                mascaraDeArquivoMascaraDeRegistro.setMascaraDeArquivo(mascaraDeArquivo);
            }
            em.persist(mascaraDeArquivoMascaraDeRegistro);
            if (mascaraDeRegistro != null) {
                mascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection().add(mascaraDeArquivoMascaraDeRegistro);
                mascaraDeRegistro = em.merge(mascaraDeRegistro);
            }
            if (mascaraDeArquivo != null) {
                mascaraDeArquivo.getMascaraDeArquivoMascaraDeRegistroCollection().add(mascaraDeArquivoMascaraDeRegistro);
                mascaraDeArquivo = em.merge(mascaraDeArquivo);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMascaraDeArquivoMascaraDeRegistro(mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK()) != null) {
                throw new PreexistingEntityException("MascaraDeArquivoMascaraDeRegistro " + mascaraDeArquivoMascaraDeRegistro + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistro) throws NonexistentEntityException, Exception {
        mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK().setNumVersaoMascaraDeArquivo(mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivo().getMascaraDeArquivoPK().getNumVersaoMascaraDeArquivo());
        mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK().setNumVersaoMascaraDeRegistro(mascaraDeArquivoMascaraDeRegistro.getMascaraDeRegistro().getMascaraDeRegistroPK().getNumVersaoMascaraDeRegistro());
        mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK().setCodMascaraDeRegistro(mascaraDeArquivoMascaraDeRegistro.getMascaraDeRegistro().getMascaraDeRegistroPK().getCodMascaraDeRegistro());
        mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK().setCodMascaraDeArquivo(mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivo().getMascaraDeArquivoPK().getCodMascaraDeArquivo());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MascaraDeArquivoMascaraDeRegistro persistentMascaraDeArquivoMascaraDeRegistro = em.find(MascaraDeArquivoMascaraDeRegistro.class, mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK());
            MascaraDeRegistro mascaraDeRegistroOld = persistentMascaraDeArquivoMascaraDeRegistro.getMascaraDeRegistro();
            MascaraDeRegistro mascaraDeRegistroNew = mascaraDeArquivoMascaraDeRegistro.getMascaraDeRegistro();
            MascaraDeArquivo mascaraDeArquivoOld = persistentMascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivo();
            MascaraDeArquivo mascaraDeArquivoNew = mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivo();
            if (mascaraDeRegistroNew != null) {
                mascaraDeRegistroNew = em.getReference(mascaraDeRegistroNew.getClass(), mascaraDeRegistroNew.getMascaraDeRegistroPK());
                mascaraDeArquivoMascaraDeRegistro.setMascaraDeRegistro(mascaraDeRegistroNew);
            }
            if (mascaraDeArquivoNew != null) {
                mascaraDeArquivoNew = em.getReference(mascaraDeArquivoNew.getClass(), mascaraDeArquivoNew.getMascaraDeArquivoPK());
                mascaraDeArquivoMascaraDeRegistro.setMascaraDeArquivo(mascaraDeArquivoNew);
            }
            mascaraDeArquivoMascaraDeRegistro = em.merge(mascaraDeArquivoMascaraDeRegistro);
            if (mascaraDeRegistroOld != null && !mascaraDeRegistroOld.equals(mascaraDeRegistroNew)) {
                mascaraDeRegistroOld.getMascaraDeArquivoMascaraDeRegistroCollection().remove(mascaraDeArquivoMascaraDeRegistro);
                mascaraDeRegistroOld = em.merge(mascaraDeRegistroOld);
            }
            if (mascaraDeRegistroNew != null && !mascaraDeRegistroNew.equals(mascaraDeRegistroOld)) {
                mascaraDeRegistroNew.getMascaraDeArquivoMascaraDeRegistroCollection().add(mascaraDeArquivoMascaraDeRegistro);
                mascaraDeRegistroNew = em.merge(mascaraDeRegistroNew);
            }
            if (mascaraDeArquivoOld != null && !mascaraDeArquivoOld.equals(mascaraDeArquivoNew)) {
                mascaraDeArquivoOld.getMascaraDeArquivoMascaraDeRegistroCollection().remove(mascaraDeArquivoMascaraDeRegistro);
                mascaraDeArquivoOld = em.merge(mascaraDeArquivoOld);
            }
            if (mascaraDeArquivoNew != null && !mascaraDeArquivoNew.equals(mascaraDeArquivoOld)) {
                mascaraDeArquivoNew.getMascaraDeArquivoMascaraDeRegistroCollection().add(mascaraDeArquivoMascaraDeRegistro);
                mascaraDeArquivoNew = em.merge(mascaraDeArquivoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                MascaraDeArquivoMascaraDeRegistroPK id = mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK();
                if (findMascaraDeArquivoMascaraDeRegistro(id) == null) {
                    throw new NonexistentEntityException("The mascaraDeArquivoMascaraDeRegistro with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(MascaraDeArquivoMascaraDeRegistroPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistro;
            try {
                mascaraDeArquivoMascaraDeRegistro = em.getReference(MascaraDeArquivoMascaraDeRegistro.class, id);
                mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mascaraDeArquivoMascaraDeRegistro with id " + id + " no longer exists.", enfe);
            }
            MascaraDeRegistro mascaraDeRegistro = mascaraDeArquivoMascaraDeRegistro.getMascaraDeRegistro();
            if (mascaraDeRegistro != null) {
                mascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection().remove(mascaraDeArquivoMascaraDeRegistro);
                mascaraDeRegistro = em.merge(mascaraDeRegistro);
            }
            MascaraDeArquivo mascaraDeArquivo = mascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivo();
            if (mascaraDeArquivo != null) {
                mascaraDeArquivo.getMascaraDeArquivoMascaraDeRegistroCollection().remove(mascaraDeArquivoMascaraDeRegistro);
                mascaraDeArquivo = em.merge(mascaraDeArquivo);
            }
            em.remove(mascaraDeArquivoMascaraDeRegistro);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MascaraDeArquivoMascaraDeRegistro> findMascaraDeArquivoMascaraDeRegistroEntities() {
        return findMascaraDeArquivoMascaraDeRegistroEntities(true, -1, -1);
    }

    public List<MascaraDeArquivoMascaraDeRegistro> findMascaraDeArquivoMascaraDeRegistroEntities(int maxResults, int firstResult) {
        return findMascaraDeArquivoMascaraDeRegistroEntities(false, maxResults, firstResult);
    }

    private List<MascaraDeArquivoMascaraDeRegistro> findMascaraDeArquivoMascaraDeRegistroEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MascaraDeArquivoMascaraDeRegistro.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MascaraDeArquivoMascaraDeRegistro findMascaraDeArquivoMascaraDeRegistro(MascaraDeArquivoMascaraDeRegistroPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MascaraDeArquivoMascaraDeRegistro.class, id);
        } finally {
            em.close();
        }
    }

    public int getMascaraDeArquivoMascaraDeRegistroCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MascaraDeArquivoMascaraDeRegistro> rt = cq.from(MascaraDeArquivoMascaraDeRegistro.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
