/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador;

import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.IllegalOrphanException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.NonexistentEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.controlador.exceptions.PreexistingEntityException;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeRegistro;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeRegistroPK;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeArquivoMascaraDeRegistro;
import java.util.ArrayList;
import java.util.Collection;
import br.com.unimed.importador.cadastro.mascara_de_arquivo.persistencia.MascaraDeCampo;

/**
 *
 * @author paulo
 */
public class MascaraDeRegistroJpaController {

    public MascaraDeRegistroJpaController() {
        emf = Persistence.createEntityManagerFactory("Importador-CadPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MascaraDeRegistro mascaraDeRegistro) throws PreexistingEntityException, Exception {
        if (mascaraDeRegistro.getMascaraDeRegistroPK() == null) {
            mascaraDeRegistro.setMascaraDeRegistroPK(new MascaraDeRegistroPK());
        }
        if (mascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection() == null) {
            mascaraDeRegistro.setMascaraDeArquivoMascaraDeRegistroCollection(new ArrayList<MascaraDeArquivoMascaraDeRegistro>());
        }
        if (mascaraDeRegistro.getMascaraDeCampoCollection() == null) {
            mascaraDeRegistro.setMascaraDeCampoCollection(new ArrayList<MascaraDeCampo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<MascaraDeArquivoMascaraDeRegistro> attachedMascaraDeArquivoMascaraDeRegistroCollection = new ArrayList<MascaraDeArquivoMascaraDeRegistro>();
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistroToAttach : mascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection()) {
                mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistroToAttach = em.getReference(mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistroToAttach.getClass(), mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistroToAttach.getMascaraDeArquivoMascaraDeRegistroPK());
                attachedMascaraDeArquivoMascaraDeRegistroCollection.add(mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistroToAttach);
            }
            mascaraDeRegistro.setMascaraDeArquivoMascaraDeRegistroCollection(attachedMascaraDeArquivoMascaraDeRegistroCollection);
            Collection<MascaraDeCampo> attachedMascaraDeCampoCollection = new ArrayList<MascaraDeCampo>();
            for (MascaraDeCampo mascaraDeCampoCollectionMascaraDeCampoToAttach : mascaraDeRegistro.getMascaraDeCampoCollection()) {
                mascaraDeCampoCollectionMascaraDeCampoToAttach = em.getReference(mascaraDeCampoCollectionMascaraDeCampoToAttach.getClass(), mascaraDeCampoCollectionMascaraDeCampoToAttach.getMascaraDeCampoPK());
                attachedMascaraDeCampoCollection.add(mascaraDeCampoCollectionMascaraDeCampoToAttach);
            }
            mascaraDeRegistro.setMascaraDeCampoCollection(attachedMascaraDeCampoCollection);
            em.persist(mascaraDeRegistro);
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro : mascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection()) {
                MascaraDeRegistro oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro = mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro.getMascaraDeRegistro();
                mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro.setMascaraDeRegistro(mascaraDeRegistro);
                mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro = em.merge(mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro);
                if (oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro != null) {
                    oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection().remove(mascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro);
                    oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro = em.merge(oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionMascaraDeArquivoMascaraDeRegistro);
                }
            }
            for (MascaraDeCampo mascaraDeCampoCollectionMascaraDeCampo : mascaraDeRegistro.getMascaraDeCampoCollection()) {
                MascaraDeRegistro oldMascaraDeRegistroOfMascaraDeCampoCollectionMascaraDeCampo = mascaraDeCampoCollectionMascaraDeCampo.getMascaraDeRegistro();
                mascaraDeCampoCollectionMascaraDeCampo.setMascaraDeRegistro(mascaraDeRegistro);
                mascaraDeCampoCollectionMascaraDeCampo = em.merge(mascaraDeCampoCollectionMascaraDeCampo);
                if (oldMascaraDeRegistroOfMascaraDeCampoCollectionMascaraDeCampo != null) {
                    oldMascaraDeRegistroOfMascaraDeCampoCollectionMascaraDeCampo.getMascaraDeCampoCollection().remove(mascaraDeCampoCollectionMascaraDeCampo);
                    oldMascaraDeRegistroOfMascaraDeCampoCollectionMascaraDeCampo = em.merge(oldMascaraDeRegistroOfMascaraDeCampoCollectionMascaraDeCampo);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMascaraDeRegistro(mascaraDeRegistro.getMascaraDeRegistroPK()) != null) {
                throw new PreexistingEntityException("MascaraDeRegistro " + mascaraDeRegistro + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MascaraDeRegistro mascaraDeRegistro) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MascaraDeRegistro persistentMascaraDeRegistro = em.find(MascaraDeRegistro.class, mascaraDeRegistro.getMascaraDeRegistroPK());
            Collection<MascaraDeArquivoMascaraDeRegistro> mascaraDeArquivoMascaraDeRegistroCollectionOld = persistentMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection();
            Collection<MascaraDeArquivoMascaraDeRegistro> mascaraDeArquivoMascaraDeRegistroCollectionNew = mascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection();
            Collection<MascaraDeCampo> mascaraDeCampoCollectionOld = persistentMascaraDeRegistro.getMascaraDeCampoCollection();
            Collection<MascaraDeCampo> mascaraDeCampoCollectionNew = mascaraDeRegistro.getMascaraDeCampoCollection();
            List<String> illegalOrphanMessages = null;
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionOldMascaraDeArquivoMascaraDeRegistro : mascaraDeArquivoMascaraDeRegistroCollectionOld) {
                if (!mascaraDeArquivoMascaraDeRegistroCollectionNew.contains(mascaraDeArquivoMascaraDeRegistroCollectionOldMascaraDeArquivoMascaraDeRegistro)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MascaraDeArquivoMascaraDeRegistro " + mascaraDeArquivoMascaraDeRegistroCollectionOldMascaraDeArquivoMascaraDeRegistro + " since its mascaraDeRegistro field is not nullable.");
                }
            }
            for (MascaraDeCampo mascaraDeCampoCollectionOldMascaraDeCampo : mascaraDeCampoCollectionOld) {
                if (!mascaraDeCampoCollectionNew.contains(mascaraDeCampoCollectionOldMascaraDeCampo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MascaraDeCampo " + mascaraDeCampoCollectionOldMascaraDeCampo + " since its mascaraDeRegistro field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<MascaraDeArquivoMascaraDeRegistro> attachedMascaraDeArquivoMascaraDeRegistroCollectionNew = new ArrayList<MascaraDeArquivoMascaraDeRegistro>();
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistroToAttach : mascaraDeArquivoMascaraDeRegistroCollectionNew) {
                mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistroToAttach = em.getReference(mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistroToAttach.getClass(), mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistroToAttach.getMascaraDeArquivoMascaraDeRegistroPK());
                attachedMascaraDeArquivoMascaraDeRegistroCollectionNew.add(mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistroToAttach);
            }
            mascaraDeArquivoMascaraDeRegistroCollectionNew = attachedMascaraDeArquivoMascaraDeRegistroCollectionNew;
            mascaraDeRegistro.setMascaraDeArquivoMascaraDeRegistroCollection(mascaraDeArquivoMascaraDeRegistroCollectionNew);
            Collection<MascaraDeCampo> attachedMascaraDeCampoCollectionNew = new ArrayList<MascaraDeCampo>();
            for (MascaraDeCampo mascaraDeCampoCollectionNewMascaraDeCampoToAttach : mascaraDeCampoCollectionNew) {
                mascaraDeCampoCollectionNewMascaraDeCampoToAttach = em.getReference(mascaraDeCampoCollectionNewMascaraDeCampoToAttach.getClass(), mascaraDeCampoCollectionNewMascaraDeCampoToAttach.getMascaraDeCampoPK());
                attachedMascaraDeCampoCollectionNew.add(mascaraDeCampoCollectionNewMascaraDeCampoToAttach);
            }
            mascaraDeCampoCollectionNew = attachedMascaraDeCampoCollectionNew;
            mascaraDeRegistro.setMascaraDeCampoCollection(mascaraDeCampoCollectionNew);
            mascaraDeRegistro = em.merge(mascaraDeRegistro);
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro : mascaraDeArquivoMascaraDeRegistroCollectionNew) {
                if (!mascaraDeArquivoMascaraDeRegistroCollectionOld.contains(mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro)) {
                    MascaraDeRegistro oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro = mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro.getMascaraDeRegistro();
                    mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro.setMascaraDeRegistro(mascaraDeRegistro);
                    mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro = em.merge(mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro);
                    if (oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro != null && !oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro.equals(mascaraDeRegistro)) {
                        oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection().remove(mascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro);
                        oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro = em.merge(oldMascaraDeRegistroOfMascaraDeArquivoMascaraDeRegistroCollectionNewMascaraDeArquivoMascaraDeRegistro);
                    }
                }
            }
            for (MascaraDeCampo mascaraDeCampoCollectionNewMascaraDeCampo : mascaraDeCampoCollectionNew) {
                if (!mascaraDeCampoCollectionOld.contains(mascaraDeCampoCollectionNewMascaraDeCampo)) {
                    MascaraDeRegistro oldMascaraDeRegistroOfMascaraDeCampoCollectionNewMascaraDeCampo = mascaraDeCampoCollectionNewMascaraDeCampo.getMascaraDeRegistro();
                    mascaraDeCampoCollectionNewMascaraDeCampo.setMascaraDeRegistro(mascaraDeRegistro);
                    mascaraDeCampoCollectionNewMascaraDeCampo = em.merge(mascaraDeCampoCollectionNewMascaraDeCampo);
                    if (oldMascaraDeRegistroOfMascaraDeCampoCollectionNewMascaraDeCampo != null && !oldMascaraDeRegistroOfMascaraDeCampoCollectionNewMascaraDeCampo.equals(mascaraDeRegistro)) {
                        oldMascaraDeRegistroOfMascaraDeCampoCollectionNewMascaraDeCampo.getMascaraDeCampoCollection().remove(mascaraDeCampoCollectionNewMascaraDeCampo);
                        oldMascaraDeRegistroOfMascaraDeCampoCollectionNewMascaraDeCampo = em.merge(oldMascaraDeRegistroOfMascaraDeCampoCollectionNewMascaraDeCampo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                MascaraDeRegistroPK id = mascaraDeRegistro.getMascaraDeRegistroPK();
                if (findMascaraDeRegistro(id) == null) {
                    throw new NonexistentEntityException("The mascaraDeRegistro with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(MascaraDeRegistroPK id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MascaraDeRegistro mascaraDeRegistro;
            try {
                mascaraDeRegistro = em.getReference(MascaraDeRegistro.class, id);
                mascaraDeRegistro.getMascaraDeRegistroPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mascaraDeRegistro with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<MascaraDeArquivoMascaraDeRegistro> mascaraDeArquivoMascaraDeRegistroCollectionOrphanCheck = mascaraDeRegistro.getMascaraDeArquivoMascaraDeRegistroCollection();
            for (MascaraDeArquivoMascaraDeRegistro mascaraDeArquivoMascaraDeRegistroCollectionOrphanCheckMascaraDeArquivoMascaraDeRegistro : mascaraDeArquivoMascaraDeRegistroCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This MascaraDeRegistro (" + mascaraDeRegistro + ") cannot be destroyed since the MascaraDeArquivoMascaraDeRegistro " + mascaraDeArquivoMascaraDeRegistroCollectionOrphanCheckMascaraDeArquivoMascaraDeRegistro + " in its mascaraDeArquivoMascaraDeRegistroCollection field has a non-nullable mascaraDeRegistro field.");
            }
            Collection<MascaraDeCampo> mascaraDeCampoCollectionOrphanCheck = mascaraDeRegistro.getMascaraDeCampoCollection();
            for (MascaraDeCampo mascaraDeCampoCollectionOrphanCheckMascaraDeCampo : mascaraDeCampoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This MascaraDeRegistro (" + mascaraDeRegistro + ") cannot be destroyed since the MascaraDeCampo " + mascaraDeCampoCollectionOrphanCheckMascaraDeCampo + " in its mascaraDeCampoCollection field has a non-nullable mascaraDeRegistro field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(mascaraDeRegistro);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MascaraDeRegistro> findMascaraDeRegistroEntities() {
        return findMascaraDeRegistroEntities(true, -1, -1);
    }

    public List<MascaraDeRegistro> findMascaraDeRegistroEntities(int maxResults, int firstResult) {
        return findMascaraDeRegistroEntities(false, maxResults, firstResult);
    }

    private List<MascaraDeRegistro> findMascaraDeRegistroEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MascaraDeRegistro.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MascaraDeRegistro findMascaraDeRegistro(MascaraDeRegistroPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MascaraDeRegistro.class, id);
        } finally {
            em.close();
        }
    }

    public int getMascaraDeRegistroCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MascaraDeRegistro> rt = cq.from(MascaraDeRegistro.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
