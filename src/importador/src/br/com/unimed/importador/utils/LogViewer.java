/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * LogViewer.java
 *
 * Created on 16/12/2010, 17:39:10
 */
package br.com.unimed.importador.utils;

import br.com.unimed.importador.persistencia.io.DataIOFactory;
import br.com.unimed.importador.persistencia.io.InterfaceIODataAccess;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * Classe responsável pela visualização dos logs
 * @author Equipe Affirmare
 */
public class LogViewer extends javax.swing.JFrame implements WindowListener {

    private DataIOFactory ioLineFactory;
    private InterfaceIODataAccess ioAccessLine;
    private String logTxt = new String();
    private String linha = new String();

    /** Creates new form LogViewer */
    public LogViewer(String file) {
        initComponents();

        openFile(file);

        this.setLocationRelativeTo(null);
        String readFi = readFile(false);

        setTextArea(readFi);

        pack();



    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(br.com.unimed.importador.visao.Main.class).getContext().getResourceMap(LogViewer.class);
        setTitle(resourceMap.getString("Form.title")); // NOI18N
        setLocationByPlatform(true);
        setMinimumSize(new java.awt.Dimension(800, 500));
        setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        setName("Form"); // NOI18N
        setResizable(false);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jTextArea1.setColumns(20);
        jTextArea1.setEditable(false);
        jTextArea1.setRows(5);
        jTextArea1.setDoubleBuffered(true);
        jTextArea1.setName("jTextArea1"); // NOI18N
        jScrollPane1.setViewportView(jTextArea1);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jLabel1.setFont(resourceMap.getFont("jLabel1.font")); // NOI18N
        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N
        getContentPane().add(jLabel1, java.awt.BorderLayout.PAGE_END);

        jMenuBar1.setName("jMenuBar1"); // NOI18N

        jMenu1.setText(resourceMap.getString("jMenu1.text")); // NOI18N
        jMenu1.setName("jMenu1"); // NOI18N

        jMenuItem2.setText(resourceMap.getString("jMenuItem2.text")); // NOI18N
        jMenuItem2.setName("jMenuItem2"); // NOI18N
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem1.setText(resourceMap.getString("jMenuItem1.text")); // NOI18N
        jMenuItem1.setName("jMenuItem1"); // NOI18N
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText(resourceMap.getString("jMenu2.text")); // NOI18N
        jMenu2.setName("jMenu2"); // NOI18N

        jMenuItem3.setText(resourceMap.getString("jMenuItem3.text")); // NOI18N
        jMenuItem3.setName("jMenuItem3"); // NOI18N
        jMenu2.add(jMenuItem3);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void setTextArea(String txt) {
        jTextArea1.setText(txt);
    }

    private String readFile(boolean selecionaType) {

        logTxt = "";

        if (ioAccessLine == null) {
            return "";
        }

        try {
            while ((linha = ioAccessLine.readLine()) != null) {
                if (selecionaType == true) {
                    if (!LoggerFile.getType().isEmpty()) {
                        for (int j = 0; j < LoggerFile.getType().size(); j++) {
                            if (linha.startsWith(LoggerFile.getNameType(j))) {
                                logTxt += linha + "\n";
                            }
                        }
                    } else {
                        logTxt += linha + "\n";
                    }
                } else {
                    logTxt += linha + "\n";
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LogViewer.class.getName()).log(Level.SEVERE, null, ex);
        }

        /* fecha a conexão com o arquivo */
        ioAccessLine.closeConnection(ioAccessLine.READ_ASC);

        return logTxt;

    }

    /**
     * Abrir um novo arquivo de log
     */
    public void openFile() {
        /* prepara a leitura do arquivo */

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int result = fileChooser.showOpenDialog(null);

        if (result == JFileChooser.CANCEL_OPTION) {
            return;
        }

        File fileName = fileChooser.getSelectedFile();

        if (fileName == null || fileName.getName().length() == 0) {
            JOptionPane.showMessageDialog(null, "Nome de Arquivo inválido", "Erro",
                    JOptionPane.ERROR_MESSAGE);

        } else {

            setup_line((fileName.getAbsolutePath()));
        }

        /* conecta no arquivo */
        ioAccessLine.connect(ioAccessLine.READ_ASC);

        /* ler o arquivo */
        String conteudo = readFile(false);

        /* apresentar o conteúdo na textarea */
        setTextArea(conteudo);

        /* fechar o arquivo */
        ioAccessLine.closeConnection(ioAccessLine.READ_ASC);


    }

    private void openFile(String file) {
        /* prepara a leitura do arquivo */

        setup_line(file);

        /* conecta no arquivo */
        ioAccessLine.connect(ioAccessLine.READ_ASC);
    }

    private void closeFile() {
    }

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        openFile();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void setup_line(String nomeDoArquivoDeOrigem) {
        // Especifica o tipo de operação (read / write)
        ioLineFactory =
                DataIOFactory.getIOFactory(DataIOFactory.READBYTE);

        // ioAccessLine permite setar o arquivo e conectar fisicamente e fechar a conexão
        ioAccessLine = ioLineFactory.getIO();

        //definir o arquivo;
        try {
            ioAccessLine.setFile(new File(nomeDoArquivoDeOrigem));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LogViewer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
         ioAccessLine.closeConnection(ioAccessLine.READ_ASC);
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }


}
