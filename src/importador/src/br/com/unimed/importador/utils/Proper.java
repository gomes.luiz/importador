/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author paulo
 */
public class Proper {
    // Read properties file.

    private static Properties properties = new Properties();

    public Proper()  {
    }

    public static Properties loadFile(String file) throws IOException {
        try {
            properties.load(new FileInputStream(file));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Proper.class.getName()).log(Level.SEVERE, null, ex);
        }

        return properties;
    }

    public static String getValue(String key) {
      
         return properties.getProperty(key);
    }
}
