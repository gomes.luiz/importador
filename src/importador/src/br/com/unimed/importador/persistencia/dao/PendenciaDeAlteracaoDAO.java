package br.com.unimed.importador.persistencia.dao;

import br.com.unimed.importador.dominio.PendenciaDeAlteracao;
import br.com.unimed.importador.exceptions.QueryException;
import br.com.unimed.importador.utils.Proper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PendenciaDeAlteracaoDAO {
   private static Connection conexao = null;
   private static Statement comando;
   private static ResultSet tuplas;
   private static PendenciaDeAlteracaoDAO instancia;

   static final String CONSULTA_PADRAO =
            "SELECT p.num_sequencial_pendencia"
            + ", p.cod_mascara_de_campo"
            + ", p.cod_mascara_de_registro"
            + ", p.num_versao_mascara_de_registro"
            + ", p.cod_tipo_de_pendencia"
            + ", p.ind_pendencia_resolvida"
            + " FROM pendencia_de_alteracao p";


   public static synchronized PendenciaDeAlteracaoDAO getInstance() {
      if (instancia == null) {
         instancia = new PendenciaDeAlteracaoDAO();
      }
      return instancia;
   }

   public List<PendenciaDeAlteracao> listaNaoResolvidas(final String codigoDaMascaraDeRegistro,
            final int numeroDeVersaoDaMascaraDeRegistro,
            final String codigoDaMascaraDeCampo){

      StringBuilder consulta = new StringBuilder(CONSULTA_PADRAO)
              .append(' ')
              .append("WHERE")
              .append(' ')
              .append(" cod_mascara_de_registro = ")
              .append("'")
              .append(codigoDaMascaraDeRegistro)
              .append("'")
              .append(" AND")
              .append(" num_versao_mascara_de_registro =")
              .append(numeroDeVersaoDaMascaraDeRegistro)
              .append(' ')
              .append(" AND")             
              .append(" cod_mascara_de_campo =")
              .append("'")
              .append(codigoDaMascaraDeCampo)
               .append("'")
              .append(" AND")
              .append(" ind_pendencia_resolvida = 0")
              .append(' ')
              .append("ORDER BY num_sequencial_pendencia");

      return lista(consulta.toString());
   }


   private List<PendenciaDeAlteracao> lista(String consulta) {
      if (consulta == null) {
         throw new IllegalArgumentException("a consulta de pendências de alteração nao pode ser nula.");
      }
      try {
         tuplas = comando.executeQuery(consulta);
         if (!tuplas.isAfterLast()){
            List<PendenciaDeAlteracao> resultado = new ArrayList<PendenciaDeAlteracao>();
            while (tuplas.next()) {
               resultado.add(new PendenciaDeAlteracao(tuplas.getInt(1), tuplas.getInt(5)
                       , tuplas.getBoolean(6)));
            }
            return resultado;
         }
      } catch (SQLException sqlException) {
         throw new QueryException("erro ao recuperar as pendências de alteracao.", sqlException);
      }
      return Collections.emptyList();
   }

   public boolean fecharConexao() {
      try {
         if (tuplas != null) {
            tuplas.close();
         }
         if (conexao != null) {
            conexao.close();
         }
         comando = null;
      } catch (SQLException ex) {
         ex.getSQLState();
         ex.getErrorCode();
      }
      return true;
   }

   public boolean abrirConexao() {
      try {
         Proper.loadFile("base.ini");
      } catch (IOException ex) {
         Logger.getLogger(MascaraDeCampoDAO.class.getName()).log(Level.SEVERE, null, ex);
      }

      conexao = MySqlDataAccessFactory.createConnection(
              Proper.getValue("usuario"), Proper.getValue("senha"),
              Proper.getValue("servidor"), Proper.getValue("banco"));
      try {
         comando = conexao.createStatement();
      } catch (SQLException ex) {
         Logger.getLogger(MascaraDeCampoDAO.class.getName()).log(Level.SEVERE, null, ex);
         return false;
      }

      return true;
   }

    public void atualizar(String instrucao) {
      try {
         comando.executeUpdate(instrucao);
      } catch (SQLException sqlException) {
         throw new QueryException("Error ao atualizar pendência de alteração", sqlException);
      }
      return;
   }

   public void mudaEstadoDasPendenciasDeAlteracaoParaResolvida(String codigo, int versao) {
      StringBuilder instrucao = new StringBuilder("UPDATE pendencia_de_alteracao SET ind_pendencia_resolvida=1");

      instrucao.append(' ')
              .append("WHERE")
              .append(' ')
              .append("cod_mascara_de_registro")
              .append("='")
              .append(codigo)
              .append("'")
              .append(' ')
              .append("AND")
              .append(' ')
              .append("num_versao_mascara_de_registro")
              .append("=")
              .append(versao);

      this.atualizar(instrucao.toString());
   }
}
