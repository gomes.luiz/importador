/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.persistencia.io;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Interface para configuração do arquivo para operação de I/O
 * @author Equipe Affirmare
 */
public interface InterfaceConfigureFile {

     // list of operations supported by the factory
    public static final int READ_FILE  = 100;
    public static final int WRITE_FILE = 101;
    public static final int READ_ASC = 102;

    
    /**
     * Especifica um arquivo para operação de I/O
     * @param io diretório e nome do arquivo
     * @throws FileNotFoundException
     */
    public  void setFile(File ioFile) throws FileNotFoundException ;


    /**
     * Retorna uma instância de File para operação de I/O
     * @return File
     */
    public  File getFile();

   /**
    * Cria um Input ou Output de acordo com type informado;
    * @param type write ou read
    * @return true se sucesso, falso caso contrário
    */
    public boolean connect(int type);

     /**
    * Cria um Input ou Output de acordo com type informado;
    * @param type write ou read
    * @param append true se anexa bytes ao arquivo de saída, falso se apaga conteúdo.
    * @return true se sucesso, falso caso contrário
    */
    public boolean connect(int type,boolean append);

    /**
     * Fecha a conexão com o arquivo
     * @param type fechar arquivo de leitura ou escrita (READ_FILE ou WRITE_FILE)
     * @return true se sucesso, falso caso contrário.
     */
    public boolean closeConnection(int type);

    /**
     * Fecha todas as conexões abertas
     * @return true se sucesso, falso caso contrário
     */
    public boolean closeConnection();

}
