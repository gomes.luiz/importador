
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.unimed.importador.persistencia.dao;

import java.util.Map;

/**
 * Classe abstrata responsável pelas operações de inserir, remover, atualizar
 * da base de dados construida pelo método factory.
 * @author affirmare
 */
public abstract class RegisterAbstractDAO implements InterfaceJdbcDataAccess {

 
     public int inserir(String sql) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean remover(String sql) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public InterfaceJdbcDataAccess listarPorChave(String id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

     public Map<String, InterfaceJdbcDataAccess> listarTodos() {

         throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean atualizar(String sql) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

     /**
     * Fecha a conexão e libera os recursos
     * @return <b> true </b> se ocorreu com sucesso
     */
    public boolean fecharConexao() {
          throw new UnsupportedOperationException("Not supported yet.");
       
    }

    public boolean abrirConexao() {
         throw new UnsupportedOperationException("Not supported yet.");
    }


 }
