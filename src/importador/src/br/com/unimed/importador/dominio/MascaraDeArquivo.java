package br.com.unimed.importador.dominio;

import br.com.unimed.importador.exceptions.RegistroDesconhecidoException;
import java.util.*;

/**
 * Classe que representa máscara de um arquivo e incluindo as máscaras
 * de registros associadas.
 *
 * @author Affirmare Tecnologia da Informação
 *
 * @version %I%, %G%
 *
 * @since 1.0
 *
 */
public class MascaraDeArquivo {

   private String codigo;
   private int versao;
   private String descricao;
   private List<MascaraDeCampo> parametros;
   private Map<String, MascaraDeRegistro> mascarasDeRegistros;

   /**
    * Construtor da classe MascaraDeArquivo.
    *
    * @param codigo código do arquivo atribuido pelo usuário.
    * @param descricao descrição do arquivo
    * @param novaCategoria categoria do arquivo (ARQUIVO ou TABELA).
    */
   public MascaraDeArquivo(final String codigo, final int versao,
           final String descricao) {
      this.codigo = codigo.toUpperCase(Locale.getDefault());
      this.versao = versao;
      this.descricao = descricao;
      this.mascarasDeRegistros = Collections.emptyMap();
      this.parametros = Collections.emptyList();
   }

   /**
    * Adiciona uma máscara de registro no arquivo.
    *
    * @param mascaraDeRegistro máscara do registro.
    */
   public final void adicionaMascaraDeRegistro(
           final MascaraDeRegistro mr) {
      if (this.mascarasDeRegistros.isEmpty()) {
         this.mascarasDeRegistros = new HashMap<String, MascaraDeRegistro>();
      }
      this.mascarasDeRegistros.put(mr.getCodigo()+mr.getVersao(), mr);
      adicionaParametro(mr);
   }



   /**
    * Retorna a mascara de registro correspondente ao registro do arquivo.
    *
    * @param linha     registro do arquivo lido.
    * @param chaves    chaves dos registros do arquivo.
    *
    * @return uma mascara de arquivo ou null se a máscara não for encontrada.
    */
   private MascaraDeRegistro obtemMascaraDeRegistro(
           final String linha, final Set<String> chaves) {
      MascaraDeRegistro resultado = null;
      MascaraDeRegistro mascara;
      String tipo;
      int tamanho, inicio, fim;

      for (String chave : chaves) {
         mascara = this.mascarasDeRegistros.get(chave);
         if (chaves.size() == 1) {
            resultado = mascara;
            break;
         }
         tamanho = linha.length();
         inicio = mascara.getPosicaoInicialDoTipoDoRegistro() - 1;
         fim = mascara.getPosicaoFinalDoTipoDoRegistro();
         if ((inicio <= fim) && (fim <= tamanho)) {
            tipo = linha.substring(inicio, fim);
            if (mascara.getCodigo().equalsIgnoreCase(tipo)) {
               resultado = mascara;
               break;
            }
         }
      }
      return resultado;
   }

   /**
    * Retorna uma instancia de MascaraDeRegistro com a atribuição de valores
    * aos campos.
    *
    * @param linhaDoArquivo   linha lida do arquivo.
    *
    * @return uma instância de máscara de arquivo.
    */
   public final MascaraDeRegistro obtemRegistroComValores(
           final String linhaDoArquivo) {
      Set<String> chaves;
      MascaraDeRegistro resultado;
      chaves = this.mascarasDeRegistros.keySet();

      resultado = this.obtemMascaraDeRegistro(linhaDoArquivo, chaves);
      if (resultado == null) {
         throw new RegistroDesconhecidoException(String.format("Registro desconhecido <%s>", linhaDoArquivo));
      }
      return resultado.obtemRegistroComValores(linhaDoArquivo);
   }


   /**
    * Adiciona um parametro a mascara de arquivo
    *
    * @param campo  Campo definido como parametro.
    */
   private void adicionaParametro(MascaraDeRegistro mr) {
      if (this.parametros.isEmpty()) {
         this.parametros = new ArrayList<MascaraDeCampo>();
      }
      for (MascaraDeCampo mc : mr.getCampos()) {
         if (mc.getTipoDeOrigem() == TipoDeOrigem.PARAMETRO) {
            parametros.add(mc);
         }
      }
   }

   /**
    * Atualiza os campos que são parametros do usuário.
    *
    * @return  vetor parâmetros.
    */
   public void atualizaParametrosDoUsuario(List<MascaraDeCampo> parametrosDoUsuario) {
      for (MascaraDeCampo mc : getParametrosDoUsuario()) {
         int i = parametrosDoUsuario.indexOf(mc);
         if (i != -1) {
            MascaraDeCampo pu = parametrosDoUsuario.get(i);
            mc.setValor(pu.getValor());
            Set<String> chavesDeRegistros = getMascarasDeRegistros().keySet();
            for (String chaveDeRegistro : chavesDeRegistros) {              
               mesclaParametroDoUsuarioNoRegistro(mascarasDeRegistros.get(chaveDeRegistro) , mc);
            }
         }        
      }
   }

   private void mesclaParametroDoUsuarioNoRegistro(MascaraDeRegistro mascaraDeRegistro,
           MascaraDeCampo parametro) {
      if (mascaraDeRegistro.getCampos().contains(parametro)) {
         int i = mascaraDeRegistro.getCampos().indexOf(parametro);
         MascaraDeCampo mc = mascaraDeRegistro.getCampos().get(i);
         mc.setValor(parametro.getValor());
      } else {
         mascaraDeRegistro.adicionaCampo(parametro);
      }
   }

   /**
    * Retorna as máscaras de registros associadas a máscara de arquivo.
    *
    * @return uma coleção contendo as máscaras de arquivos associadas.
    */
   public final List<MascaraDeCampo> getParametrosDoUsuario() {
      return this.parametros;
   }

   /**
    * Retorna as máscaras de registros associadas a máscara de arquivo.
    *
    * @return uma coleção contendo as máscaras de arquivos associadas.
    */
   public final Map<String, MascaraDeRegistro> getMascarasDeRegistros() {
      return this.mascarasDeRegistros;
   }

   /**
    * Retorna a descrição da máscara de arquivo.
    *
    * @return  descrição da máscara de arquivo.
    */
   public final String getDescricao() {
      return this.descricao;
   }

   /**
    * Retorna o código da máscara de arquivo.
    *
    * @return  o código da máscara de arquivo.
    */
   public final String getCodigo() {
      return this.codigo;
   }

   public int getVersao() {
      return versao;
   }

   
}
