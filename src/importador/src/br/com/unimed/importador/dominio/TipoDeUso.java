/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.unimed.importador.dominio;

/**
 *
 * @author luiz
 */
public enum TipoDeUso {
    OPCIONAL , MANDATORIO;

      public static TipoDeUso getTipoDeUso(int number) {

        switch (number) {
            case 1:
                return TipoDeUso.OPCIONAL;
            case 0:
                return TipoDeUso.MANDATORIO;

        }

        return null;

    }

}
